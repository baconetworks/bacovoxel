package com.thevoxelbox.voxelsniper.jsap.util;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class StringUtils
{
  public StringUtils() {}
  
  public static String noNull(String paramString)
  {
    return paramString == null ? "" : paramString;
  }
  
  public static String padRight(String paramString, int paramInt)
  {
    if (paramInt < 0) {
      throw new IllegalArgumentException("padCount must be >= 0");
    }
    StringBuffer localStringBuffer = new StringBuffer(noNull(paramString));
    for (int i = 0; i < paramInt; i++) {
      localStringBuffer.append(' ');
    }
    return localStringBuffer.toString();
  }
  
  public static String padRightToWidth(String paramString, int paramInt)
  {
    String str = noNull(paramString);
    if (str.length() < paramInt) {
      str = padRight(str, paramInt - str.length());
    }
    return str;
  }
  
  public static List wrapToList(String paramString, int paramInt)
  {
    LinkedList localLinkedList = new LinkedList();
    if ((paramString != null) && (paramString.length() > 0))
    {
      StringBuffer localStringBuffer = new StringBuffer();
      int i = -1;
      for (int j = 0; j < paramString.length(); j++)
      {
        char c = paramString.charAt(j);
        if (c == '\n')
        {
          localLinkedList.add(localStringBuffer.toString());
          localStringBuffer.setLength(0);
          i = -1;
        }
        else if (c == ' ')
        {
          if (localStringBuffer.length() >= paramInt - 1)
          {
            localLinkedList.add(localStringBuffer.toString());
            localStringBuffer.setLength(0);
            i = -1;
          }
          if (localStringBuffer.length() > 0)
          {
            i = localStringBuffer.length();
            localStringBuffer.append(c);
          }
        }
        else
        {
          if ((localStringBuffer.length() >= paramInt) && (i != -1))
          {
            localLinkedList.add(localStringBuffer.substring(0, i));
            localStringBuffer.delete(0, i + 1);
            i = -1;
          }
          localStringBuffer.append(c);
        }
      }
      if (localStringBuffer.length() > 0) {
        localLinkedList.add(localStringBuffer.toString());
      }
    }
    return localLinkedList;
  }
  
  public static void main(String[] paramArrayOfString)
  {
    String str1 = "This is\n a test that I would like to word wrap at 15 characters.";
    System.out.println("123456789012345");
    List localList = wrapToList(str1, 15);
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      String str2 = (String)localIterator.next();
      System.out.println(str2 + "|");
    }
  }
}
