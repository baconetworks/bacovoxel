package com.thevoxelbox.voxelsniper.jsap.jsap;

import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.BooleanStringParser;
import java.util.ArrayList;
import java.util.List;

public class Switch
  extends Parameter
  implements Flagged
{
  private char shortFlag = '\000';
  private String longFlag = JSAP.NO_LONGFLAG;
  
  public Switch(String paramString)
  {
    super(paramString);
    setDefault("FALSE");
  }
  
  public Switch(String paramString1, char paramChar, String paramString2, String paramString3)
  {
    this(paramString1);
    setShortFlag(paramChar);
    setLongFlag(paramString2);
    setHelp(paramString3);
  }
  
  public Switch(String paramString1, char paramChar, String paramString2)
  {
    this(paramString1, paramChar, paramString2, JSAP.NO_HELP);
  }
  
  public Switch setShortFlag(char paramChar)
  {
    enforceParameterLock();
    shortFlag = paramChar;
    return this;
  }
  
  public char getShortFlag()
  {
    return shortFlag;
  }
  
  public Character getShortFlagCharacter()
  {
    return shortFlag == 0 ? null : new Character(shortFlag);
  }
  
  public Switch setLongFlag(String paramString)
  {
    enforceParameterLock();
    longFlag = paramString;
    return this;
  }
  
  public String getLongFlag()
  {
    return longFlag;
  }
  
  protected List parse(String paramString)
    throws ParseException
  {
    ArrayList localArrayList = new ArrayList(1);
    localArrayList.add(BooleanStringParser.getParser().parse(paramString));
    return localArrayList;
  }
  
  public String getSyntax()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int i = 0;
    localStringBuffer.append("[");
    if (getShortFlag() != 0)
    {
      localStringBuffer.append("-" + getShortFlag());
      i = 1;
    }
    if (getLongFlag() != JSAP.NO_LONGFLAG)
    {
      if (i != 0) {
        localStringBuffer.append("|");
      }
      localStringBuffer.append("--" + getLongFlag());
    }
    localStringBuffer.append("]");
    return localStringBuffer.toString();
  }
  
  public Switch setDefault(String paramString)
  {
    _setDefault(paramString);
    return this;
  }
  
  public Switch setDefault(String[] paramArrayOfString)
  {
    _setDefault(paramArrayOfString);
    return this;
  }
}
