package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class ClassStringParser
  extends StringParser
{
  private static final ClassStringParser INSTANCE = new ClassStringParser();
  
  public static ClassStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public ClassStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    Class localClass = null;
    try
    {
      localClass = Class.forName(paramString);
    }
    catch (Exception localException)
    {
      throw new ParseException("Unable to locate class '" + paramString + "'.", localException);
    }
    return localClass;
  }
}
