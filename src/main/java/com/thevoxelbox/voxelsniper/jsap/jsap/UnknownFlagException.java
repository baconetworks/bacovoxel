package com.thevoxelbox.voxelsniper.jsap.jsap;

public class UnknownFlagException
  extends JSAPException
{
  private String flag = null;
  
  public UnknownFlagException(String paramString)
  {
    super("Unknown flag '" + paramString + "'.");
    flag = paramString;
  }
  
  public UnknownFlagException(Character paramCharacter)
  {
    super("Unknown flag '" + paramCharacter + "'.");
    flag = paramCharacter.toString();
  }
  
  public String getFlag()
  {
    return flag;
  }
}
