package com.thevoxelbox.voxelsniper.jsap.jsap;

import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.BigDecimalStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.BigIntegerStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.BooleanStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.ByteStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.CharacterStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.ClassStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.ColorStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.DoubleStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.FloatStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.InetAddressStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.IntSizeStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.IntegerStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.LongSizeStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.LongStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.PackageStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.ShortStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.StringStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.URLStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.xml.JSAPConfig;
import com.thevoxelbox.voxelsniper.jsap.util.StringUtils;
import com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers.BooleanStringParser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JSAP
{
  private Map paramsByID = null;
  private Map paramsByShortFlag = null;
  private Map paramsByLongFlag = null;
  private List unflaggedOptions = null;
  private List paramsByDeclarationOrder = null;
  private List defaultSources = null;
  private String usage = null;
  private String help = null;
  public static final char NO_SHORTFLAG = '\000';
  public static final String NO_LONGFLAG = null;
  public static final char DEFAULT_LISTSEPARATOR = File.pathSeparatorChar;
  public static final String DEFAULT_PARAM_HELP_SEPARATOR = "\n";
  public static final boolean REQUIRED = true;
  public static final boolean NOT_REQUIRED = false;
  public static final boolean LIST = true;
  public static final boolean NOT_LIST = false;
  public static final boolean MULTIPLEDECLARATIONS = true;
  public static final boolean NO_MULTIPLEDECLARATIONS = false;
  public static final boolean GREEDY = true;
  public static final boolean NOT_GREEDY = false;
  public static final String NO_DEFAULT = null;
  public static final String NO_HELP = null;
  public static final BigDecimalStringParser BIGDECIMAL_PARSER = BigDecimalStringParser.getParser();
  public static final BigIntegerStringParser BIGINTEGER_PARSER = BigIntegerStringParser.getParser();
  public static final BooleanStringParser BOOLEAN_PARSER = BooleanStringParser.getParser();
  public static final ByteStringParser BYTE_PARSER = ByteStringParser.getParser();
  public static final CharacterStringParser CHARACTER_PARSER = CharacterStringParser.getParser();
  public static final ClassStringParser CLASS_PARSER = ClassStringParser.getParser();
  public static final ColorStringParser COLOR_PARSER = ColorStringParser.getParser();
  public static final DoubleStringParser DOUBLE_PARSER = DoubleStringParser.getParser();
  public static final FloatStringParser FLOAT_PARSER = FloatStringParser.getParser();
  public static final InetAddressStringParser INETADDRESS_PARSER = InetAddressStringParser.getParser();
  public static final IntegerStringParser INTEGER_PARSER = IntegerStringParser.getParser();
  public static final IntSizeStringParser INTSIZE_PARSER = IntSizeStringParser.getParser();
  public static final LongSizeStringParser LONGSIZE_PARSER = LongSizeStringParser.getParser();
  public static final LongStringParser LONG_PARSER = LongStringParser.getParser();
  public static final PackageStringParser PACKAGE_PARSER = PackageStringParser.getParser();
  public static final ShortStringParser SHORT_PARSER = ShortStringParser.getParser();
  public static final StringStringParser STRING_PARSER = StringStringParser.getParser();
  public static final URLStringParser URL_PARSER = URLStringParser.getParser();
  public static final int DEFAULT_SCREENWIDTH = 80;
  static char SYNTAX_SPACECHAR = ' ';
  
  public JSAP()
  {
    init();
  }
  
  public JSAP(URL paramURL)
    throws IOException, JSAPException
  {
    init();
    JSAPConfig.configure(this, paramURL);
  }
  
  public JSAP(String paramString)
    throws IOException, JSAPException
  {
    this(JSAP.class.getClassLoader().getResource(paramString));
  }
  
  private void init()
  {
    paramsByID = new HashMap();
    paramsByShortFlag = new HashMap();
    paramsByLongFlag = new HashMap();
    unflaggedOptions = new ArrayList();
    paramsByDeclarationOrder = new ArrayList();
    defaultSources = new ArrayList();
  }
  
  public void setUsage(String paramString)
  {
    usage = paramString;
  }
  
  public void setHelp(String paramString)
  {
    help = paramString;
  }
  
  public String getHelp()
  {
    return getHelp(80, "\n");
  }
  
  public String getHelp(int paramInt)
  {
    return getHelp(paramInt, "\n");
  }
  
  public String getHelp(int paramInt, String paramString)
  {
    String str = help;
    if (str == null)
    {
      StringBuffer localStringBuffer1 = new StringBuffer();
      int i = paramInt - 8;
      Iterator localIterator1 = paramsByDeclarationOrder.iterator();
      while (localIterator1.hasNext())
      {
        Parameter localParameter = (Parameter)localIterator1.next();
        StringBuffer localStringBuffer2 = new StringBuffer();
        String[] arrayOfString = localParameter.getDefault();
        if ((!(localParameter instanceof Switch)) && (arrayOfString != null))
        {
          localStringBuffer2.append(" (default: ");
          for (int j = 0; j < arrayOfString.length; j++)
          {
            if (j > 0) {
              localStringBuffer2.append(", ");
            }
            localStringBuffer2.append(arrayOfString[j]);
          }
          localStringBuffer2.append(")");
        }
        Iterator localIterator2 = StringUtils.wrapToList(localParameter.getHelp() + localStringBuffer2, i).iterator();
        localStringBuffer1.append("  ");
        localStringBuffer1.append(localParameter.getSyntax());
        localStringBuffer1.append("\n");
        while (localIterator2.hasNext())
        {
          localStringBuffer1.append("        ");
          localStringBuffer1.append(localIterator2.next());
          localStringBuffer1.append("\n");
        }
        if (localIterator1.hasNext()) {
          localStringBuffer1.append(paramString);
        }
      }
      str = localStringBuffer1.toString();
    }
    return str;
  }
  
  public String getUsage()
  {
    String str = usage;
    if (str == null)
    {
      StringBuffer localStringBuffer = new StringBuffer();
      Iterator localIterator = paramsByDeclarationOrder.iterator();
      while (localIterator.hasNext())
      {
        Parameter localParameter = (Parameter)localIterator.next();
        if (localStringBuffer.length() > 0) {
          localStringBuffer.append(" ");
        }
        localStringBuffer.append(localParameter.getSyntax());
      }
      str = localStringBuffer.toString();
    }
    return str;
  }
  
  public String toString()
  {
    return getUsage();
  }
  
  public IDMap getIDMap()
  {
    ArrayList localArrayList = new ArrayList(paramsByDeclarationOrder.size());
    Object localObject1 = paramsByDeclarationOrder.iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Parameter localObject2 = (Parameter) ((Iterator) localObject1).next();
      localArrayList.add(((Parameter)localObject2).getID());
    }
    localObject1 = new HashMap();
    Object localObject2 = paramsByShortFlag.keySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Character localObject3 = (Character) ((Iterator) localObject2).next();
      ((Map)localObject1).put(localObject3, ((Parameter)paramsByShortFlag.get(localObject3)).getID());
    }
    localObject2 = new HashMap();
    Object localObject3 = paramsByLongFlag.keySet().iterator();
    while (((Iterator)localObject3).hasNext())
    {
      String str = (String)((Iterator)localObject3).next();
      ((Map)localObject2).put(str, ((Parameter)paramsByLongFlag.get(str)).getID());
    }
    return new IDMap(localArrayList, (Map)localObject1, (Map)localObject2);
  }
  
  public Parameter getByID(String paramString)
  {
    return (Parameter)paramsByID.get(paramString);
  }
  
  public Flagged getByLongFlag(String paramString)
  {
    return (Flagged)paramsByLongFlag.get(paramString);
  }
  
  public Flagged getByShortFlag(Character paramCharacter)
  {
    return (Flagged)paramsByShortFlag.get(paramCharacter);
  }
  
  public Flagged getByShortFlag(char paramChar)
  {
    return getByShortFlag(new Character(paramChar));
  }
  
  public Iterator getUnflaggedOptionsIterator()
  {
    return unflaggedOptions.iterator();
  }
  
  public void registerDefaultSource(DefaultSource paramDefaultSource)
  {
    defaultSources.add(paramDefaultSource);
  }
  
  public void unregisterDefaultSource(DefaultSource paramDefaultSource)
  {
    defaultSources.remove(paramDefaultSource);
  }
  
  private Defaults getSystemDefaults()
  {
    Defaults localDefaults = new Defaults();
    Iterator localIterator = paramsByDeclarationOrder.iterator();
    while (localIterator.hasNext())
    {
      Parameter localParameter = (Parameter)localIterator.next();
      localDefaults.setDefault(localParameter.getID(), localParameter.getDefault());
    }
    return localDefaults;
  }
  
  private void combineDefaults(Defaults paramDefaults1, Defaults paramDefaults2)
  {
    if (paramDefaults2 != null)
    {
      Iterator localIterator = paramDefaults2.idIterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        paramDefaults1.setDefaultIfNeeded(str, paramDefaults2.getDefault(str));
      }
    }
  }
  
  protected Defaults getDefaults(ExceptionMap paramExceptionMap)
  {
    Defaults localDefaults = new Defaults();
    IDMap localIDMap = getIDMap();
    Iterator localIterator = defaultSources.iterator();
    while (localIterator.hasNext())
    {
      DefaultSource localDefaultSource = (DefaultSource)localIterator.next();
      combineDefaults(localDefaults, localDefaultSource.getDefaults(localIDMap, paramExceptionMap));
    }
    combineDefaults(localDefaults, getSystemDefaults());
    return localDefaults;
  }
  
  public void registerParameter(Parameter paramParameter)
    throws JSAPException
  {
    String str = paramParameter.getID();
    if (paramsByID.containsKey(str)) {
      throw new JSAPException("A parameter with ID '" + str + "' has already been registered.");
    }
    Flagged localFlagged;
    if ((paramParameter instanceof Flagged))
    {
      localFlagged = (Flagged)paramParameter;
      if ((localFlagged.getShortFlagCharacter() == null) && (localFlagged.getLongFlag() == null)) {
        throw new JSAPException("FlaggedOption '" + str + "' has no flags defined.");
      }
      if (paramsByShortFlag.containsKey(localFlagged.getShortFlagCharacter())) {
        throw new JSAPException("A parameter with short flag '" + localFlagged.getShortFlag() + "' has already been registered.");
      }
      if (paramsByLongFlag.containsKey(localFlagged.getLongFlag())) {
        throw new JSAPException("A parameter with long flag '" + localFlagged.getLongFlag() + "' has already been registered.");
      }
    }
    else if ((unflaggedOptions.size() > 0) && (((UnflaggedOption)unflaggedOptions.get(unflaggedOptions.size() - 1)).isGreedy()))
    {
      throw new JSAPException("A greedy unflagged option has already been registered; option '" + str + "' will never be reached.");
    }
    if ((paramParameter instanceof Option)) {
      ((Option)paramParameter).register();
    }
    paramParameter.setLocked(true);
    paramsByID.put(str, paramParameter);
    paramsByDeclarationOrder.add(paramParameter);
    if ((paramParameter instanceof Flagged))
    {
      localFlagged = (Flagged)paramParameter;
      if (localFlagged.getShortFlagCharacter() != null) {
        paramsByShortFlag.put(localFlagged.getShortFlagCharacter(), paramParameter);
      }
      if (localFlagged.getLongFlag() != null) {
        paramsByLongFlag.put(localFlagged.getLongFlag(), paramParameter);
      }
    }
    else if ((paramParameter instanceof Option))
    {
      unflaggedOptions.add(paramParameter);
    }
  }
  
  public void unregisterParameter(Parameter paramParameter)
  {
    if (paramsByID.containsKey(paramParameter.getID()))
    {
      if ((paramParameter instanceof Option)) {
        ((Option)paramParameter).unregister();
      }
      paramsByID.remove(paramParameter.getID());
      paramsByDeclarationOrder.remove(paramParameter);
      if ((paramParameter instanceof Flagged))
      {
        Flagged localFlagged = (Flagged)paramParameter;
        paramsByShortFlag.remove(localFlagged.getShortFlagCharacter());
        paramsByLongFlag.remove(localFlagged.getLongFlag());
      }
      else if ((paramParameter instanceof UnflaggedOption))
      {
        unflaggedOptions.remove(paramParameter);
      }
      paramParameter.setLocked(false);
    }
  }
  
  public JSAPResult parse(String[] paramArrayOfString)
  {
    Parser localParser = new Parser(this, paramArrayOfString);
    return localParser.parse();
  }
  
  public JSAPResult parse(String paramString)
  {
    String[] arrayOfString = CommandLineTokenizer.tokenize(paramString);
    return parse(arrayOfString);
  }
  
  public void finalize()
  {
    Parameter[] arrayOfParameter = (Parameter[])paramsByDeclarationOrder.toArray(new Parameter[0]);
    int i = arrayOfParameter.length;
    for (int j = 0; j < i; j++) {
      unregisterParameter(arrayOfParameter[j]);
    }
  }
  
  static
  {
    if (Boolean.valueOf(System.getProperty("com.thevoxelbox.voxelsniper.jsap.jsap.usenbsp", "false")).booleanValue()) {
    }
  }
}
