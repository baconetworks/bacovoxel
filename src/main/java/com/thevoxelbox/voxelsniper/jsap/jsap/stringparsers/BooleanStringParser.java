package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class BooleanStringParser
  extends StringParser
{
  private static final BooleanStringParser INSTANCE = new BooleanStringParser();
  
  public static BooleanStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public BooleanStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    boolean bool = false;
    if ((paramString == null) || (paramString.equalsIgnoreCase("t")) || (paramString.equalsIgnoreCase("true")) || (paramString.equalsIgnoreCase("y")) || (paramString.equalsIgnoreCase("yes")) || (paramString.equals("1"))) {
      bool = true;
    } else if ((paramString.equalsIgnoreCase("f")) || (paramString.equalsIgnoreCase("false")) || (paramString.equalsIgnoreCase("n")) || (paramString.equalsIgnoreCase("no")) || (paramString.equals("0"))) {
      bool = false;
    } else {
      throw new ParseException("Unable to convert '" + paramString + "' to a boolean value.");
    }
    return new Boolean(bool);
  }
}
