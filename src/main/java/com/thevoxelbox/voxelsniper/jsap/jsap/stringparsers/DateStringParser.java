package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.PropertyStringParser;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateStringParser
  extends PropertyStringParser
{
  private SimpleDateFormat format = null;
  
  public static DateStringParser getParser()
  {
    return new DateStringParser();
  }
  
  /**
   * @deprecated
   */
  public DateStringParser() {}
  
  public void setUp()
    throws com.thevoxelbox.voxelsniper.jsap.jsap.ParseException
  {
    String str = getProperty("format");
    if (str == null) {
      format = new SimpleDateFormat();
    } else {
      try
      {
        format = new SimpleDateFormat(str);
      }
      catch (RuntimeException localRuntimeException)
      {
        throw new com.thevoxelbox.voxelsniper.jsap.jsap.ParseException(localRuntimeException);
      }
    }
  }
  
  public void tearDown()
  {
    format = null;
  }
  
  public Object parse(String paramString)
    throws com.thevoxelbox.voxelsniper.jsap.jsap.ParseException
  {
    Date localDate = null;
    try
    {
      localDate = format.parse(paramString);
    }
    catch (java.text.ParseException localParseException)
    {
      throw new com.thevoxelbox.voxelsniper.jsap.jsap.ParseException("Unable to convert '" + paramString + "' to a Date.", localParseException);
    }
    return localDate;
  }
}
