package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class DoubleStringParser
  extends StringParser
{
  private static final DoubleStringParser INSTANCE = new DoubleStringParser();
  
  public static DoubleStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public DoubleStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    Double localDouble = null;
    try
    {
      localDouble = new Double(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to a Double.", localNumberFormatException);
    }
    return localDouble;
  }
}
