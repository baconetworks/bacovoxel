package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.PropertyStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.util.Iterator;
import java.util.List;

class StringParserConfig
{
  String classname = null;
  List properties = null;
  
  StringParserConfig() {}
  
  public String getClassname()
  {
    return classname;
  }
  
  public void setClassname(String paramString)
  {
    classname = paramString;
  }
  
  public List getProperties()
  {
    return properties;
  }
  
  public void setProperties(List paramList)
  {
    properties = paramList;
  }
  
  public StringParser getConfiguredStringParser()
  {
    try
    {
      StringParser localStringParser = null;
      if (classname.indexOf('.') >= 0) {
        localStringParser = (StringParser)Class.forName(classname).newInstance();
      } else {
        localStringParser = (StringParser)Class.forName("com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers." + classname).newInstance();
      }
      if ((properties != null) && (properties.size() > 0))
      {
        PropertyStringParser localPropertyStringParser = (PropertyStringParser)localStringParser;
        Iterator localIterator = properties.iterator();
        while (localIterator.hasNext())
        {
          Property localProperty = (Property)localIterator.next();
          localPropertyStringParser.setProperty(localProperty.getName(), localProperty.getValue());
        }
      }
      return localStringParser;
    }
    catch (Throwable localThrowable)
    {
      throw new RuntimeException("Unable to create StringParser " + classname + ": " + localThrowable.getMessage(), localThrowable);
    }
  }
}
