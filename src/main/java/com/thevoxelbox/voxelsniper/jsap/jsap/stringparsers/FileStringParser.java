package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.PropertyStringParser;
import java.io.File;

public class FileStringParser
  extends PropertyStringParser
{
  public static final String MUSTBEFILE = "mustBeFile";
  public static final String MUSTBEDIRECTORY = "mustBeDirectory";
  public static final String MUSTEXIST = "mustExist";
  private boolean mustExist = false;
  private boolean mustBeDirectory = false;
  private boolean mustBeFile = false;
  
  /**
   * @deprecated
   */
  public FileStringParser() {}
  
  public static FileStringParser getParser()
  {
    return new FileStringParser();
  }
  
  public void setUp()
    throws ParseException
  {
    BooleanStringParser localBooleanStringParser = JSAP.BOOLEAN_PARSER;
    setMustExist(((Boolean)localBooleanStringParser.parse(getProperty("mustExist", new Boolean(mustExist).toString()))).booleanValue());
    setMustBeDirectory(((Boolean)localBooleanStringParser.parse(getProperty("mustBeDirectory", new Boolean(mustBeDirectory).toString()))).booleanValue());
    setMustBeFile(((Boolean)localBooleanStringParser.parse(getProperty("mustBeFile", new Boolean(mustBeFile).toString()))).booleanValue());
  }
  
  public FileStringParser setMustBeDirectory(boolean paramBoolean)
  {
    mustBeDirectory = paramBoolean;
    return this;
  }
  
  public FileStringParser setMustBeFile(boolean paramBoolean)
  {
    mustBeFile = paramBoolean;
    return this;
  }
  
  public FileStringParser setMustExist(boolean paramBoolean)
  {
    mustExist = paramBoolean;
    return this;
  }
  
  public boolean mustBeDirectory()
  {
    return mustBeDirectory;
  }
  
  public boolean mustBeFile()
  {
    return mustBeFile;
  }
  
  public boolean mustExist()
  {
    return mustExist;
  }
  
  public void tearDown() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    File localFile = null;
    try
    {
      localFile = new File(paramString);
      if ((mustExist()) && (!localFile.exists())) {
        throw new ParseException(localFile + " does not exist.");
      }
      if ((mustBeDirectory()) && (localFile.exists()) && (!localFile.isDirectory())) {
        throw new ParseException(localFile + " is not a directory.");
      }
      if ((mustBeFile()) && (localFile.exists()) && (localFile.isDirectory())) {
        throw new ParseException(localFile + " is not a file.");
      }
    }
    catch (NullPointerException localNullPointerException)
    {
      throw new ParseException("No File given to parse", localNullPointerException);
    }
    return localFile;
  }
}
