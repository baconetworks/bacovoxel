package com.thevoxelbox.voxelsniper.jsap.jsap;

public abstract interface ExceptionMap
{
  public abstract void addException(String paramString, Exception paramException);
  
  public abstract Exception getException(String paramString);
  
  public abstract Exception[] getExceptionArray(String paramString);
}
