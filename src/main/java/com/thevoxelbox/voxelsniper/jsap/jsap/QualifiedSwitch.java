package com.thevoxelbox.voxelsniper.jsap.jsap;

public final class QualifiedSwitch
  extends FlaggedOption
{
  public QualifiedSwitch(String paramString1, StringParser paramStringParser, String paramString2, boolean paramBoolean, char paramChar, String paramString3, String paramString4)
  {
    super(paramString1, paramStringParser, paramString2, paramBoolean, paramChar, paramString3, paramString4);
  }
  
  public QualifiedSwitch(String paramString1, StringParser paramStringParser, String paramString2, boolean paramBoolean, char paramChar, String paramString3)
  {
    super(paramString1, paramStringParser, paramString2, paramBoolean, paramChar, paramString3);
  }
  
  public QualifiedSwitch(String paramString)
  {
    super(paramString);
  }
  
  public String getSyntax()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    if (!required()) {
      localStringBuffer.append("[");
    }
    if ((getLongFlag() != JSAP.NO_LONGFLAG) || (getShortFlag() != 0)) {
      if (getLongFlag() == JSAP.NO_LONGFLAG) {
        localStringBuffer.append("-" + getShortFlag());
      } else if (getShortFlag() == 0) {
        localStringBuffer.append("--" + getLongFlag());
      } else {
        localStringBuffer.append("(-" + getShortFlag() + "|--" + getLongFlag() + ")");
      }
    }
    localStringBuffer.append("[:");
    String str = getUsageName();
    char c = getListSeparator();
    if (isList()) {
      localStringBuffer.append(str + "1" + c + str + "2" + c + "..." + c + str + "N ");
    } else {
      localStringBuffer.append("<" + str + ">");
    }
    if (!required()) {
      localStringBuffer.append("]");
    }
    localStringBuffer.append("]");
    return localStringBuffer.toString();
  }
}
