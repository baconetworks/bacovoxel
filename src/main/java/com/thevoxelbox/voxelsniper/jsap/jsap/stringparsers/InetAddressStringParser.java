package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressStringParser
  extends StringParser
{
  private static final InetAddressStringParser INSTANCE = new InetAddressStringParser();
  
  public static InetAddressStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public InetAddressStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    InetAddress localInetAddress = null;
    try
    {
      localInetAddress = InetAddress.getByName(paramString);
    }
    catch (UnknownHostException localUnknownHostException)
    {
      throw new ParseException("Unknown host: " + paramString, localUnknownHostException);
    }
    return localInetAddress;
  }
}
