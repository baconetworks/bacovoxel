package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.awt.Color;
import java.util.StringTokenizer;

public class ColorStringParser
  extends StringParser
{
  private static final ColorStringParser INSTANCE = new ColorStringParser();
  
  public static ColorStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public ColorStringParser() {}
  
  private ParseException colorException(String paramString)
  {
    return new ParseException("Unable to convert '" + paramString + "' to a Color.");
  }
  
  private Color parseHexColor(String paramString)
    throws ParseException
  {
    Color localColor = null;
    int i = paramString.length();
    if ((i != 7) && (i != 9)) {
      throw colorException(paramString);
    }
    try
    {
      int j = Integer.parseInt(paramString.substring(1, 3), 16);
      int k = Integer.parseInt(paramString.substring(3, 5), 16);
      int m = Integer.parseInt(paramString.substring(5, 7), 16);
      if (i == 7)
      {
        localColor = new Color(j, k, m);
      }
      else
      {
        int n = Integer.parseInt(paramString.substring(7, 9), 16);
        localColor = new Color(j, k, m, n);
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw colorException(paramString);
    }
    return localColor;
  }
  
  private Color parseFloatTuple(String paramString)
    throws ParseException
  {
    String[] arrayOfString = tupleToArray(paramString);
    Color localColor = null;
    try
    {
      float f1 = Float.parseFloat(arrayOfString[0]);
      float f2 = Float.parseFloat(arrayOfString[1]);
      float f3 = Float.parseFloat(arrayOfString[2]);
      if (arrayOfString.length == 3)
      {
        localColor = new Color(f1, f2, f3);
      }
      else
      {
        float f4 = Float.parseFloat(arrayOfString[3]);
        localColor = new Color(f1, f2, f3, f4);
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw colorException(paramString);
    }
    return localColor;
  }
  
  private Color parseIntTuple(String paramString)
    throws ParseException
  {
    String[] arrayOfString = tupleToArray(paramString);
    Color localColor = null;
    try
    {
      int i = Integer.parseInt(arrayOfString[0]);
      int j = Integer.parseInt(arrayOfString[1]);
      int k = Integer.parseInt(arrayOfString[2]);
      if (arrayOfString.length == 3)
      {
        localColor = new Color(i, j, k);
      }
      else
      {
        int m = Integer.parseInt(arrayOfString[3]);
        localColor = new Color(i, j, k, m);
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw colorException(paramString);
    }
    return localColor;
  }
  
  private Color parseColorName(String paramString)
    throws ParseException
  {
    Color localColor = null;
    if (paramString.equalsIgnoreCase("black")) {
      localColor = Color.black;
    } else if (paramString.equalsIgnoreCase("blue")) {
      localColor = Color.blue;
    } else if (paramString.equalsIgnoreCase("cyan")) {
      localColor = Color.cyan;
    } else if (paramString.equalsIgnoreCase("gray")) {
      localColor = Color.gray;
    } else if (paramString.equalsIgnoreCase("green")) {
      localColor = Color.green;
    } else if (paramString.equalsIgnoreCase("lightGray")) {
      localColor = Color.lightGray;
    } else if (paramString.equalsIgnoreCase("magenta")) {
      localColor = Color.magenta;
    } else if (paramString.equalsIgnoreCase("orange")) {
      localColor = Color.orange;
    } else if (paramString.equalsIgnoreCase("pink")) {
      localColor = Color.pink;
    } else if (paramString.equalsIgnoreCase("red")) {
      localColor = Color.red;
    } else if (paramString.equalsIgnoreCase("white")) {
      localColor = Color.white;
    } else if (paramString.equalsIgnoreCase("yellow")) {
      localColor = Color.yellow;
    } else {
      throw colorException(paramString);
    }
    return localColor;
  }
  
  private String[] tupleToArray(String paramString)
    throws ParseException
  {
    String[] arrayOfString = null;
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",", true);
    int i = localStringTokenizer.countTokens();
    if (i == 5) {
      arrayOfString = new String[3];
    } else if (i == 7) {
      arrayOfString = new String[4];
    } else {
      throw colorException(paramString);
    }
    arrayOfString[0] = localStringTokenizer.nextToken();
    localStringTokenizer.nextToken();
    arrayOfString[1] = localStringTokenizer.nextToken();
    localStringTokenizer.nextToken();
    arrayOfString[2] = localStringTokenizer.nextToken();
    if (i == 7)
    {
      localStringTokenizer.nextToken();
      arrayOfString[3] = localStringTokenizer.nextToken();
    }
    return arrayOfString;
  }
  
  public Object parse(String paramString)
    throws ParseException
  {
    Color localColor = null;
    if (paramString.charAt(0) == '#') {
      localColor = parseHexColor(paramString);
    } else if (paramString.indexOf(".") >= 0) {
      localColor = parseFloatTuple(paramString);
    } else if (paramString.indexOf(",") >= 0) {
      localColor = parseIntTuple(paramString);
    } else {
      localColor = parseColorName(paramString);
    }
    return localColor;
  }
}
