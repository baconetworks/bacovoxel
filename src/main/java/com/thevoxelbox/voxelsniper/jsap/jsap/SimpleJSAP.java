package com.thevoxelbox.voxelsniper.jsap.jsap;

import com.thevoxelbox.voxelsniper.jsap.util.StringUtils;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;

public class SimpleJSAP
  extends JSAP
{
  private int screenWidth = 80;
  private String explanation;
  private boolean messagePrinted;
  private final String name;
  
  public SimpleJSAP(String paramString1, String paramString2, Parameter[] paramArrayOfParameter)
    throws JSAPException
  {
    name = paramString1;
    explanation = paramString2;
    Switch localSwitch = new Switch("help", '\000', "help");
    localSwitch.setHelp("Prints this help message.");
    registerParameter(localSwitch);
    if (paramArrayOfParameter != null) {
      for (int i = 0; i < paramArrayOfParameter.length; i++) {
        registerParameter(paramArrayOfParameter[i]);
      }
    }
  }
  
  public SimpleJSAP(String paramString1, String paramString2)
    throws JSAPException
  {
    this(paramString1, paramString2, null);
  }
  
  public SimpleJSAP(String paramString)
    throws JSAPException
  {
    this(paramString, null);
  }
  
  public JSAPResult parse(String paramString)
  {
    JSAPResult localJSAPResult = super.parse(paramString);
    messagePrinted = printMessageIfUnsuccessfulOrHelpRequired(localJSAPResult);
    return localJSAPResult;
  }
  
  public JSAPResult parse(String[] paramArrayOfString)
  {
    JSAPResult localJSAPResult = super.parse(paramArrayOfString);
    messagePrinted = printMessageIfUnsuccessfulOrHelpRequired(localJSAPResult);
    return localJSAPResult;
  }
  
  private boolean printMessageIfUnsuccessfulOrHelpRequired(JSAPResult paramJSAPResult)
  {
    if ((!paramJSAPResult.success()) || (paramJSAPResult.getBoolean("help")))
    {
      if (!paramJSAPResult.getBoolean("help"))
      {
        Iterator localObject = paramJSAPResult.getErrorMessageIterator();
        while (((Iterator)localObject).hasNext()) {
          System.err.println("Error: " + ((Iterator)localObject).next());
        }
        return true;
      }
      System.err.println();
      System.err.println("Usage:");
      Object localObject = StringUtils.wrapToList(name + " " + getUsage(), screenWidth);
      Iterator localIterator = ((List)localObject).iterator();
      while (localIterator.hasNext()) {
        System.err.println("  " + localIterator.next().toString());
      }
      if (explanation != null)
      {
        System.err.println();
        localObject = StringUtils.wrapToList(explanation, screenWidth);
        localIterator = ((List)localObject).iterator();
        while (localIterator.hasNext()) {
          System.err.println(localIterator.next());
        }
      }
      System.err.println();
      System.err.println();
      System.err.println(getHelp(screenWidth));
      return true;
    }
    return false;
  }
  
  public int getScreenWidth()
  {
    return screenWidth;
  }
  
  public SimpleJSAP setScreenWidth(int paramInt)
  {
    screenWidth = paramInt;
    return this;
  }
  
  public boolean messagePrinted()
  {
    return messagePrinted;
  }
}
