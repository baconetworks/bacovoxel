package com.thevoxelbox.voxelsniper.jsap.jsap.ant;

public class ParserProperty
{
  private String name = null;
  private String value = null;
  
  public ParserProperty() {}
  
  public void setName(String paramString)
  {
    name = paramString;
  }
  
  public String getName()
  {
    return name;
  }
  
  public void setValue(String paramString)
  {
    value = paramString;
  }
  
  public String getValue()
  {
    return value;
  }
}
