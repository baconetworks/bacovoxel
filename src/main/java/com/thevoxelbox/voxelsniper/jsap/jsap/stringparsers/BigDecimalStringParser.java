package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.math.BigDecimal;

public class BigDecimalStringParser
  extends StringParser
{
  private static final BigDecimalStringParser INSTANCE = new BigDecimalStringParser();
  
  public static BigDecimalStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public BigDecimalStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    BigDecimal localBigDecimal = null;
    try
    {
      localBigDecimal = new BigDecimal(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to a BigDecimal.", localNumberFormatException);
    }
    return localBigDecimal;
  }
}
