package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import java.io.PrintStream;
import java.util.ArrayList;

public class TryDumpXML
{
  public TryDumpXML() {}
  
  public static void main(String[] paramArrayOfString)
    throws Exception
  {
    JSAPXStream localJSAPXStream = new JSAPXStream();
    JSAPConfig localJSAPConfig = new JSAPConfig();
    FlaggedOptionConfig localFlaggedOptionConfig = new FlaggedOptionConfig();
    localFlaggedOptionConfig.setId("flagged");
    localFlaggedOptionConfig.setShortFlag('f');
    localFlaggedOptionConfig.setHelp("This flag does something, but I'm not sure what.");
    localJSAPConfig.addParameter(localFlaggedOptionConfig);
    UnflaggedOptionConfig localUnflaggedOptionConfig = new UnflaggedOptionConfig();
    localUnflaggedOptionConfig.setId("unflagged");
    localUnflaggedOptionConfig.setGreedy(true);
    StringParserConfig localStringParserConfig = new StringParserConfig();
    localStringParserConfig.setClassname("DateStringParser");
    ArrayList localArrayList = new ArrayList();
    Property localProperty = new Property();
    localProperty.setName("DateFormat");
    localProperty.setValue("MM/dd/yyyy");
    localArrayList.add(localProperty);
    localProperty = new Property();
    localProperty.setName("Another Property");
    localProperty.setValue("123");
    localArrayList.add(localProperty);
    localStringParserConfig.setProperties(localArrayList);
    localUnflaggedOptionConfig.setStringParser(localStringParserConfig);
    localJSAPConfig.addParameter(localUnflaggedOptionConfig);
    System.out.println(localJSAPXStream.toXML(localJSAPConfig));
  }
}
