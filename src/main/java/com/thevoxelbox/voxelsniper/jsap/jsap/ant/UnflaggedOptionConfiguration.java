package com.thevoxelbox.voxelsniper.jsap.jsap.ant;

import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import com.thevoxelbox.voxelsniper.jsap.jsap.UnflaggedOption;
import java.io.PrintStream;

public class UnflaggedOptionConfiguration
  extends OptionConfiguration
{
  private boolean greedy = false;
  
  public UnflaggedOptionConfiguration() {}
  
  public void setGreedy(boolean paramBoolean)
  {
    greedy = paramBoolean;
  }
  
  public boolean getGreedy()
  {
    return greedy;
  }
  
  public Parameter getParameter()
  {
    UnflaggedOption localUnflaggedOption = new UnflaggedOption(getId());
    localUnflaggedOption.setRequired(getRequired());
    localUnflaggedOption.setList(getIslist());
    if (declaredListSeparator()) {
      localUnflaggedOption.setListSeparator(getListseparator());
    }
    localUnflaggedOption.setGreedy(getGreedy());
    localUnflaggedOption.setDefault(getDefaults());
    setupStringParser(localUnflaggedOption);
    return localUnflaggedOption;
  }
  
  public void createMethod(String paramString, PrintStream paramPrintStream)
  {
    paramPrintStream.println("    private UnflaggedOption " + paramString + "() {");
    paramPrintStream.println("        UnflaggedOption result = new UnflaggedOption(\"" + getId() + "\");");
    paramPrintStream.println("        result.setGreedy(" + (getGreedy() ? "true" : "false") + ");");
    super.createParentStatements("result", paramPrintStream);
    paramPrintStream.println("        return (result);");
    paramPrintStream.println("    }");
  }
}
