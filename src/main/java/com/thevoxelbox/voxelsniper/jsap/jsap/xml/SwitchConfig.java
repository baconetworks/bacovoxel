package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import com.thevoxelbox.voxelsniper.jsap.jsap.Switch;

class SwitchConfig
  extends FlaggedConfig
{
  SwitchConfig() {}
  
  public Parameter getConfiguredParameter()
  {
    Switch localSwitch = new Switch(getId());
    super.configure(localSwitch);
    localSwitch.setShortFlag(getShortFlag());
    localSwitch.setLongFlag(getLongFlag());
    return localSwitch;
  }
}
