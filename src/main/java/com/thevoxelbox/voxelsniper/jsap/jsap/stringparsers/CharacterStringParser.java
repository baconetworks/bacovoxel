package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class CharacterStringParser
  extends StringParser
{
  private static final CharacterStringParser INSTANCE = new CharacterStringParser();
  
  public static CharacterStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public CharacterStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    if ((paramString == null) || (paramString.length() != 1)) {
      throw new ParseException("Unable to convert '" + paramString + "' to a Character.");
    }
    return new Character(paramString.charAt(0));
  }
}
