package com.thevoxelbox.voxelsniper.jsap.jsap.ant;

import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import com.thevoxelbox.voxelsniper.jsap.jsap.Switch;
import java.io.PrintStream;

public class SwitchConfiguration
  extends ParameterConfiguration
{
  private char shortFlag = '\000';
  private String longFlag = JSAP.NO_LONGFLAG;
  
  public SwitchConfiguration() {}
  
  public void setShortflag(char paramChar)
  {
    shortFlag = paramChar;
  }
  
  public char getShortflag()
  {
    return shortFlag;
  }
  
  public void setLongflag(String paramString)
  {
    longFlag = paramString;
  }
  
  public String getLongflag()
  {
    return longFlag;
  }
  
  public Parameter getParameter()
  {
    Switch localSwitch = new Switch(getId());
    localSwitch.setShortFlag(getShortflag());
    localSwitch.setLongFlag(getLongflag());
    localSwitch.setDefault(getDefaults());
    return localSwitch;
  }
  
  public void createMethod(String paramString, PrintStream paramPrintStream)
  {
    paramPrintStream.println("    private Switch " + paramString + "() {");
    paramPrintStream.println("        Switch result = new Switch(\"" + getId() + "\");");
    if (getShortflag() != 0) {
      paramPrintStream.println("        result.setShortFlag('" + getShortflag() + "');");
    }
    if (getLongflag() != JSAP.NO_LONGFLAG) {
      paramPrintStream.println("        result.setLongFlag(\"" + getLongflag() + "\");");
    }
    super.createParentStatements("result", paramPrintStream);
    paramPrintStream.println("        return (result);");
    paramPrintStream.println("    }");
  }
}
