package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

abstract class ParameterConfig
{
  private String id = null;
  private String help = null;
  private String usageName = null;
  private List defaults = null;
  
  public ParameterConfig() {}
  
  public void addDefault(String paramString)
  {
    getDefaults().add(paramString);
  }
  
  public List getDefaults()
  {
    if (defaults == null) {
      defaults = new ArrayList();
    }
    return defaults;
  }
  
  public String getHelp()
  {
    return help;
  }
  
  public void setHelp(String paramString)
  {
    help = paramString;
  }
  
  public String getId()
  {
    return id;
  }
  
  public void setId(String paramString)
  {
    id = paramString;
  }
  
  public String getUsageName()
  {
    return usageName;
  }
  
  public void setUsageName(String paramString)
  {
    usageName = paramString;
  }
  
  protected void configure(Parameter paramParameter)
  {
    paramParameter.setHelp(getHelp());
    Iterator localIterator = getDefaults().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      paramParameter.addDefault(str);
    }
  }
  
  public abstract Parameter getConfiguredParameter();
}
