package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

class Property
{
  private String name = null;
  private String value = null;
  
  Property() {}
  
  public String getName()
  {
    return name;
  }
  
  public void setName(String paramString)
  {
    name = paramString;
  }
  
  public String getValue()
  {
    return value;
  }
  
  public void setValue(String paramString)
  {
    value = paramString;
  }
}
