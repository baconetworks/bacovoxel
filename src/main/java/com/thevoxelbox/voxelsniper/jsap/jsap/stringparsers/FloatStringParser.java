package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class FloatStringParser
  extends StringParser
{
  private static final FloatStringParser INSTANCE = new FloatStringParser();
  
  public static FloatStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public FloatStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    Float localFloat = null;
    try
    {
      localFloat = new Float(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to a Float.", localNumberFormatException);
    }
    return localFloat;
  }
}
