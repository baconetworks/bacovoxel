package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class StringStringParser
  extends StringParser
{
  private static final StringStringParser INSTANCE = new StringStringParser();
  
  public static StringStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public StringStringParser() {}
  
  public Object parse(String paramString)
  {
    return paramString;
  }
}
