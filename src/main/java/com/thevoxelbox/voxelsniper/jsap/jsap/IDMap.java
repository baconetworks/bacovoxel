package com.thevoxelbox.voxelsniper.jsap.jsap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class IDMap
{
  private List ids = null;
  private Map byShortFlag = null;
  private Map byLongFlag = null;
  
  public IDMap(List paramList, Map paramMap1, Map paramMap2)
  {
    ids = new ArrayList(paramList);
    byShortFlag = new HashMap(paramMap1);
    byLongFlag = new HashMap(paramMap2);
  }
  
  public Iterator idIterator()
  {
    return ids.iterator();
  }
  
  public boolean idExists(String paramString)
  {
    return ids.contains(paramString);
  }
  
  public String getIDByShortFlag(Character paramCharacter)
  {
    return (String)byShortFlag.get(paramCharacter);
  }
  
  public String getIDByShortFlag(char paramChar)
  {
    return getIDByShortFlag(new Character(paramChar));
  }
  
  public String getIDByLongFlag(String paramString)
  {
    return (String)byLongFlag.get(paramString);
  }
}
