package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import com.thevoxelbox.voxelsniper.jsap.jsap.UnflaggedOption;

class UnflaggedOptionConfig
  extends ParameterConfig
{
  private boolean greedy = false;
  private boolean list = false;
  private char listSeparator = JSAP.DEFAULT_LISTSEPARATOR;
  private StringParserConfig stringParser = null;
  private boolean required = false;
  private String usageName = null;
  
  public UnflaggedOptionConfig() {}
  
  public boolean isGreedy()
  {
    return greedy;
  }
  
  public void setGreedy(boolean paramBoolean)
  {
    greedy = paramBoolean;
  }
  
  public boolean isList()
  {
    return list;
  }
  
  public void setList(boolean paramBoolean)
  {
    list = paramBoolean;
  }
  
  public char getListSeparator()
  {
    return listSeparator;
  }
  
  public void setListSeparator(char paramChar)
  {
    listSeparator = paramChar;
  }
  
  public boolean isRequired()
  {
    return required;
  }
  
  public void setRequired(boolean paramBoolean)
  {
    required = paramBoolean;
  }
  
  public StringParserConfig getStringParser()
  {
    return stringParser;
  }
  
  public void setStringParser(StringParserConfig paramStringParserConfig)
  {
    stringParser = paramStringParserConfig;
  }
  
  public String getUsageName()
  {
    return usageName;
  }
  
  public void setUsageName(String paramString)
  {
    usageName = paramString;
  }
  
  public Parameter getConfiguredParameter()
  {
    UnflaggedOption localUnflaggedOption = new UnflaggedOption(getId());
    super.configure(localUnflaggedOption);
    localUnflaggedOption.setGreedy(isGreedy());
    localUnflaggedOption.setListSeparator(getListSeparator());
    localUnflaggedOption.setList(isList());
    localUnflaggedOption.setRequired(isRequired());
    localUnflaggedOption.setUsageName(getUsageName());
    if (stringParser != null) {
      localUnflaggedOption.setStringParser(stringParser.getConfiguredStringParser());
    }
    return localUnflaggedOption;
  }
}
