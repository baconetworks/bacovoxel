package com.thevoxelbox.voxelsniper.jsap.jsap.ant;

import com.thevoxelbox.voxelsniper.jsap.jsap.FlaggedOption;
import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import com.thevoxelbox.voxelsniper.jsap.jsap.Option;
import com.thevoxelbox.voxelsniper.jsap.jsap.PropertyStringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import com.thevoxelbox.voxelsniper.jsap.jsap.UnflaggedOption;
import java.io.PrintStream;
import java.util.Vector;

public abstract class OptionConfiguration
  extends ParameterConfiguration
{
  private boolean required = false;
  private Vector parserProperties = null;
  private boolean isList = false;
  private char listSeparator = JSAP.DEFAULT_LISTSEPARATOR;
  private boolean declaredListSeparator = false;
  private String stringParser = null;
  
  public OptionConfiguration() {}
  
  public void setStringparser(String paramString)
  {
    stringParser = paramString;
  }
  
  public String getStringparser()
  {
    String str = null;
    if (stringParser != null) {
      if (stringParser.indexOf(".") == -1) {
        str = "com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers." + stringParser;
      } else {
        str = stringParser;
      }
    }
    return str;
  }
  
  public void setRequired(boolean paramBoolean)
  {
    required = paramBoolean;
  }
  
  public boolean getRequired()
  {
    return required;
  }
  
  public void setIslist(boolean paramBoolean)
  {
    isList = paramBoolean;
  }
  
  public boolean getIslist()
  {
    return isList;
  }
  
  public void setListseparator(char paramChar)
  {
    declaredListSeparator = true;
    listSeparator = paramChar;
  }
  
  public char getListseparator()
  {
    return listSeparator;
  }
  
  public boolean declaredListSeparator()
  {
    return declaredListSeparator;
  }
  
  public void addConfiguredProperty(ParserProperty paramParserProperty)
  {
    parserProperties.add(paramParserProperty);
  }
  
  public ParserProperty[] getParserProperties()
  {
    ParserProperty[] arrayOfParserProperty = null;
    if (parserProperties.size() > 0) {
      arrayOfParserProperty = (ParserProperty[])parserProperties.toArray(new ParserProperty[0]);
    }
    return arrayOfParserProperty;
  }
  
  public boolean hasProperties()
  {
    ParserProperty[] arrayOfParserProperty = getParserProperties();
    return (arrayOfParserProperty != null) && (arrayOfParserProperty.length > 0);
  }
  
  protected void setupStringParser(Option paramOption)
  {
    if (getStringparser() != null) {
      try
      {
        StringParser localStringParser = (StringParser)Class.forName(getStringparser()).newInstance();
        if ((paramOption instanceof FlaggedOption)) {
          ((FlaggedOption)paramOption).setStringParser(localStringParser);
        } else {
          ((UnflaggedOption)paramOption).setStringParser(localStringParser);
        }
      }
      catch (Exception localException1)
      {
        throw new IllegalArgumentException("Unable to instantiate \"" + getStringparser() + "\"");
      }
    }
    if (hasProperties()) {
      try
      {
        PropertyStringParser localPropertyStringParser = (PropertyStringParser)paramOption.getStringParser();
        ParserProperty[] arrayOfParserProperty = getParserProperties();
        for (int i = 0; i < arrayOfParserProperty.length; i++) {
          localPropertyStringParser.setProperty(arrayOfParserProperty[i].getName(), arrayOfParserProperty[i].getValue());
        }
      }
      catch (Exception localException2)
      {
        throw new IllegalArgumentException("Option \"" + paramOption.getID() + "\": " + paramOption.getStringParser().getClass().getName() + " is not an instance of " + "com.thevoxelbox.voxelsniper.jsap.jsap.PropertyParser.");
      }
    }
  }
  
  protected void createParentStatements(String paramString, PrintStream paramPrintStream)
  {
    super.createParentStatements(paramString, paramPrintStream);
    paramPrintStream.println("        " + paramString + ".setList(" + (getIslist() ? "true" : "false") + ");");
    paramPrintStream.println("        " + paramString + ".setRequired(" + (getRequired() ? "true" : "false") + ");");
    if (getStringparser() != null) {
      paramPrintStream.println("        " + paramString + ".setStringParser( new " + getStringparser() + "() );");
    }
    if (hasProperties())
    {
      ParserProperty[] arrayOfParserProperty = getParserProperties();
      paramPrintStream.println();
      paramPrintStream.println("        PropertyStringParser psp = (PropertyStringParser) " + paramString + ".getStringParser();");
      for (int i = 0; i < arrayOfParserProperty.length; i++) {
        paramPrintStream.println("        psp.setProperty(\"" + arrayOfParserProperty[i].getName() + "\", \"" + arrayOfParserProperty[i].getValue() + "\");");
      }
      paramPrintStream.println();
    }
    if (declaredListSeparator()) {
      paramPrintStream.println("        " + paramString + ".setListSeparator('" + getListseparator() + "');");
    }
  }
}
