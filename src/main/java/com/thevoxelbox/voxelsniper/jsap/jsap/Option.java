package com.thevoxelbox.voxelsniper.jsap.jsap;

import java.util.ArrayList;
import java.util.List;

public abstract class Option
  extends Parameter
{
  private boolean isList = false;
  private boolean required = false;
  private char listSeparator = JSAP.DEFAULT_LISTSEPARATOR;
  private StringParser stringParser = null;
  
  public Option(String paramString)
  {
    super(paramString);
  }
  
  protected final void internalSetList(boolean paramBoolean)
  {
    enforceParameterLock();
    isList = paramBoolean;
  }
  
  public final boolean isList()
  {
    return isList;
  }
  
  protected final void internalSetListSeparator(char paramChar)
  {
    enforceParameterLock();
    listSeparator = paramChar;
  }
  
  public final char getListSeparator()
  {
    return listSeparator;
  }
  
  protected final void internalSetRequired(boolean paramBoolean)
  {
    enforceParameterLock();
    required = paramBoolean;
  }
  
  public final boolean required()
  {
    return required;
  }
  
  protected final void internalSetStringParser(StringParser paramStringParser)
  {
    enforceParameterLock();
    stringParser = paramStringParser;
  }
  
  public final StringParser getStringParser()
  {
    return stringParser;
  }
  
  private void storeParseResult(List paramList, String paramString)
    throws ParseException
  {
    if (paramString == null) {
      return;
    }
    Object localObject = getStringParser().parse(paramString);
    if (localObject != null) {
      paramList.add(localObject);
    }
  }
  
  protected final List parse(String paramString)
    throws ParseException
  {
    ArrayList localArrayList = new ArrayList();
    if (getStringParser() == null)
    {
      boolean bool = locked();
      setLocked(false);
      internalSetStringParser(JSAP.STRING_PARSER);
      setLocked(bool);
    }
    if ((paramString == null) || (!isList()))
    {
      storeParseResult(localArrayList, paramString);
    }
    else
    {
      StringBuffer localStringBuffer = new StringBuffer();
      int i = paramString.length();
      for (int j = 0; j < i; j++)
      {
        char c = paramString.charAt(j);
        if (c == getListSeparator())
        {
          storeParseResult(localArrayList, localStringBuffer.toString());
          localStringBuffer.setLength(0);
        }
        else
        {
          localStringBuffer.append(c);
          if (j == i - 1) {
            storeParseResult(localArrayList, localStringBuffer.toString());
          }
        }
      }
    }
    return localArrayList;
  }
  
  protected void register()
    throws JSAPException
  {
    StringParser localStringParser = getStringParser();
    try
    {
      if (localStringParser != null) {
        localStringParser.setUp();
      }
    }
    catch (Exception localException)
    {
      throw new JSAPException(localException.getMessage(), localException);
    }
  }
  
  protected void unregister()
  {
    StringParser localStringParser = getStringParser();
    if (localStringParser != null) {
      localStringParser.tearDown();
    }
  }
}
