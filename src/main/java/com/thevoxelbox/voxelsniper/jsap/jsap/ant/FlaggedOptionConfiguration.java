package com.thevoxelbox.voxelsniper.jsap.jsap.ant;

import com.thevoxelbox.voxelsniper.jsap.jsap.FlaggedOption;
import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import java.io.PrintStream;

public class FlaggedOptionConfiguration
  extends OptionConfiguration
{
  private char shortFlag = '\000';
  private String longFlag = JSAP.NO_LONGFLAG;
  private boolean allowMultipleDeclarations = false;
  private boolean declaredAllowMultipleDeclarations = false;
  
  public FlaggedOptionConfiguration() {}
  
  public void setShortflag(char paramChar)
  {
    shortFlag = paramChar;
  }
  
  public char getShortflag()
  {
    return shortFlag;
  }
  
  public void setLongflag(String paramString)
  {
    longFlag = paramString;
  }
  
  public String getLongflag()
  {
    return longFlag;
  }
  
  public void setAllowmultipledeclarations(boolean paramBoolean)
  {
    allowMultipleDeclarations = paramBoolean;
    declaredAllowMultipleDeclarations = true;
  }
  
  public Parameter getParameter()
  {
    FlaggedOption localFlaggedOption = new FlaggedOption(getId());
    localFlaggedOption.setShortFlag(getShortflag());
    localFlaggedOption.setLongFlag(getLongflag());
    localFlaggedOption.setRequired(getRequired());
    localFlaggedOption.setList(getIslist());
    if (declaredListSeparator()) {
      localFlaggedOption.setListSeparator(getListseparator());
    }
    if (declaredAllowMultipleDeclarations) {
      localFlaggedOption.setAllowMultipleDeclarations(allowMultipleDeclarations);
    }
    setupStringParser(localFlaggedOption);
    localFlaggedOption.setDefault(getDefaults());
    return localFlaggedOption;
  }
  
  public void createMethod(String paramString, PrintStream paramPrintStream)
  {
    paramPrintStream.println("    private FlaggedOption " + paramString + "() {");
    paramPrintStream.println("        FlaggedOption result = new FlaggedOption(\"" + getId() + "\");");
    if (getShortflag() != 0) {
      paramPrintStream.println("        result.setShortFlag('" + getShortflag() + "');");
    }
    if (getLongflag() != JSAP.NO_LONGFLAG) {
      paramPrintStream.println("        result.setLongFlag(\"" + getLongflag() + "\");");
    }
    if (declaredAllowMultipleDeclarations) {
      paramPrintStream.println("        result.setAllowMultipleDeclarations(" + (allowMultipleDeclarations ? "true" : "false") + ");");
    }
    super.createParentStatements("result", paramPrintStream);
    paramPrintStream.println("        return (result);");
    paramPrintStream.println("    }");
  }
}
