package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class EnumeratedStringParser
  extends StringParser
{
  public static final char CONSTRUCTOR_VALUE_SEPARATOR = ';';
  private String[] validOptionValuesArray = null;
  private boolean isCaseSensitive;
  private boolean checkOptionChars;
  
  public static EnumeratedStringParser getParser(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    throws IllegalArgumentException
  {
    return new EnumeratedStringParser(paramString, paramBoolean1, paramBoolean2);
  }
  
  /**
   * @deprecated
   */
  public EnumeratedStringParser(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    throws IllegalArgumentException
  {
    if (paramString == null) {
      throw new IllegalArgumentException("EnumeratedStringParser validOptions parameter is null");
    }
    if (paramString.length() == 0) {
      throw new IllegalArgumentException("EnumeratedStringParser validOptions parameter is empty");
    }
    isCaseSensitive = paramBoolean1;
    checkOptionChars = paramBoolean2;
    if (paramString.indexOf(';') == -1)
    {
      validOptionValuesArray = new String[1];
      if (isValidOptionName(paramString)) {
        validOptionValuesArray[0] = paramString;
      } else {
        throw new IllegalArgumentException("Wrong character in EnumeratedStringParser option value: " + paramString + "\nsee EnumeratedStringParser javadoc for more information");
      }
    }
    else
    {
      StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ";");
      validOptionValuesArray = new String[localStringTokenizer.countTokens()];
      int i = 0;
      while (localStringTokenizer.hasMoreTokens())
      {
        String str = localStringTokenizer.nextToken().trim();
        if (!isCaseSensitive) {
          str = str.toLowerCase();
        }
        if (isValidOptionName(str)) {
          validOptionValuesArray[(i++)] = str;
        } else {
          throw new IllegalArgumentException("Wrong character in EnumeratedStringParser option value: " + str + "\nsee EnumeratedStringParser javadoc for more information");
        }
      }
    }
  }
  
  public static EnumeratedStringParser getParser(String paramString, boolean paramBoolean)
    throws IllegalArgumentException
  {
    return new EnumeratedStringParser(paramString, paramBoolean, true);
  }
  
  /**
   * @deprecated
   */
  public EnumeratedStringParser(String paramString, boolean paramBoolean)
    throws IllegalArgumentException
  {
    this(paramString, paramBoolean, true);
  }
  
  public static EnumeratedStringParser getParser(String paramString)
    throws IllegalArgumentException
  {
    return new EnumeratedStringParser(paramString, false, true);
  }
  
  /**
   * @deprecated
   */
  public EnumeratedStringParser(String paramString)
    throws IllegalArgumentException
  {
    this(paramString, false, true);
  }
  
  public Object parse(String paramString)
    throws ParseException
  {
    if (paramString == null) {
      return null;
    }
    if (!isCaseSensitive) {
      paramString = paramString.toLowerCase();
    }
    if (!isValidOptionName(paramString)) {
      throw new ParseException("Wrong character in command line option value for enumerated option: '" + paramString + "'" + "\nallowed are alphanumeric characters + '$' and '_' sign only", new IllegalArgumentException());
    }
    if (Arrays.asList(validOptionValuesArray).contains(paramString)) {
      return paramString;
    }
    throw new ParseException("Option has wrong value '" + paramString + "'" + "; valid values are: " + Arrays.asList(validOptionValuesArray), new IllegalArgumentException());
  }
  
  protected boolean isValidOptionName(String paramString)
  {
    if (!checkOptionChars) {
      return true;
    }
    for (int i = 0; i < paramString.length(); i++)
    {
      char c = paramString.charAt(i);
      if (!Character.isJavaIdentifierPart(c)) {
        return false;
      }
    }
    return true;
  }
}
