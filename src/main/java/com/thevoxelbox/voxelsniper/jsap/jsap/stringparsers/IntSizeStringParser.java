package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class IntSizeStringParser
  extends StringParser
{
  static final IntSizeStringParser INSTANCE = new IntSizeStringParser();
  
  private IntSizeStringParser() {}
  
  public static IntSizeStringParser getParser()
  {
    return INSTANCE;
  }
  
  public Object parse(String paramString)
    throws ParseException
  {
    long l = LongSizeStringParser.parseSize(paramString);
    if (l > 2147483647L) {
      throw new ParseException("Integer size '" + paramString + "' is too big.");
    }
    return new Integer((int)l);
  }
}
