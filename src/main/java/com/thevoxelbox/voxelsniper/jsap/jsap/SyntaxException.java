package com.thevoxelbox.voxelsniper.jsap.jsap;

public class SyntaxException
  extends JSAPException
{
  public SyntaxException(String paramString)
  {
    super(paramString);
  }
}
