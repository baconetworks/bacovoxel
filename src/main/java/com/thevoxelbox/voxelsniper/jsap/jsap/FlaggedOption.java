package com.thevoxelbox.voxelsniper.jsap.jsap;

public class FlaggedOption
  extends Option
  implements Flagged
{
  private char shortFlag = '\000';
  private String longFlag = JSAP.NO_LONGFLAG;
  private boolean allowMultipleDeclarations = false;
  
  public FlaggedOption(String paramString)
  {
    super(paramString);
  }
  
  public FlaggedOption(String paramString1, StringParser paramStringParser, String paramString2, boolean paramBoolean, char paramChar, String paramString3, String paramString4)
  {
    this(paramString1);
    setStringParser(paramStringParser);
    setDefault(paramString2);
    setShortFlag(paramChar);
    setLongFlag(paramString3);
    setRequired(paramBoolean);
    setHelp(paramString4);
  }
  
  public FlaggedOption(String paramString1, StringParser paramStringParser, String paramString2, boolean paramBoolean, char paramChar, String paramString3)
  {
    this(paramString1, paramStringParser, paramString2, paramBoolean, paramChar, paramString3, JSAP.NO_HELP);
  }
  
  public FlaggedOption setShortFlag(char paramChar)
  {
    enforceParameterLock();
    shortFlag = paramChar;
    return this;
  }
  
  public char getShortFlag()
  {
    return shortFlag;
  }
  
  public Character getShortFlagCharacter()
  {
    return shortFlag == 0 ? null : new Character(shortFlag);
  }
  
  public FlaggedOption setLongFlag(String paramString)
  {
    enforceParameterLock();
    longFlag = paramString;
    return this;
  }
  
  public FlaggedOption setUsageName(String paramString)
  {
    _setUsageName(paramString);
    return this;
  }
  
  public String getLongFlag()
  {
    return longFlag;
  }
  
  public FlaggedOption setAllowMultipleDeclarations(boolean paramBoolean)
  {
    enforceParameterLock();
    allowMultipleDeclarations = paramBoolean;
    return this;
  }
  
  public boolean allowMultipleDeclarations()
  {
    return allowMultipleDeclarations;
  }
  
  public String getSyntax()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    if (!required()) {
      localStringBuffer.append("[");
    }
    if ((getLongFlag() != JSAP.NO_LONGFLAG) || (getShortFlag() != 0)) {
      if (getLongFlag() == JSAP.NO_LONGFLAG) {
        localStringBuffer.append("-" + getShortFlag() + JSAP.SYNTAX_SPACECHAR);
      } else if (getShortFlag() == 0) {
        localStringBuffer.append("--" + getLongFlag() + JSAP.SYNTAX_SPACECHAR);
      } else {
        localStringBuffer.append("(-" + getShortFlag() + "|--" + getLongFlag() + ")" + JSAP.SYNTAX_SPACECHAR);
      }
    }
    String str = getUsageName();
    char c = getListSeparator();
    if (isList()) {
      localStringBuffer.append(str + "1" + c + str + "2" + c + "..." + c + str + "N ");
    } else {
      localStringBuffer.append("<" + getUsageName() + ">");
    }
    if (!required()) {
      localStringBuffer.append("]");
    }
    return localStringBuffer.toString();
  }
  
  public FlaggedOption setList(boolean paramBoolean)
  {
    super.internalSetList(paramBoolean);
    return this;
  }
  
  public FlaggedOption setListSeparator(char paramChar)
  {
    super.internalSetListSeparator(paramChar);
    return this;
  }
  
  public FlaggedOption setRequired(boolean paramBoolean)
  {
    super.internalSetRequired(paramBoolean);
    return this;
  }
  
  public FlaggedOption setDefault(String[] paramArrayOfString)
  {
    _setDefault(paramArrayOfString);
    return this;
  }
  
  public FlaggedOption setDefault(String paramString)
  {
    _setDefault(paramString);
    return this;
  }
  
  public FlaggedOption setStringParser(StringParser paramStringParser)
  {
    super.internalSetStringParser(paramStringParser);
    return this;
  }
}
