package com.thevoxelbox.voxelsniper.jsap.jsap;

import java.awt.Color;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JSAPResult
  implements ExceptionMap
{
  private Map allResults = null;
  private Map qualifiedSwitches = null;
  private Map allExceptions = null;
  private List chronologicalErrorMessages = null;
  private Set userSpecifiedIDs = null;
  private boolean valuesFromUser = false;
  
  protected JSAPResult() {}
  
  void setValuesFromUser(boolean paramBoolean)
  {
    valuesFromUser = paramBoolean;
  }
  
  public boolean contains(String paramString)
  {
    return allResults.containsKey(paramString);
  }
  
  public boolean userSpecified(String paramString)
  {
    return userSpecifiedIDs.contains(paramString);
  }
  
  protected void add(String paramString, List paramList)
  {
    Object localObject = null;
    if (allResults.containsKey(paramString))
    {
      localObject = (List)allResults.get(paramString);
    }
    else
    {
      localObject = new ArrayList();
      allResults.put(paramString, localObject);
    }
    ((List)localObject).addAll(paramList);
    if (valuesFromUser) {
      userSpecifiedIDs.add(paramString);
    }
  }
  
  public void addException(String paramString, Exception paramException)
  {
    Object localObject = null;
    if (allExceptions.containsKey(paramString))
    {
      localObject = (List)allExceptions.get(paramString);
    }
    else
    {
      localObject = new ArrayList();
      allExceptions.put(paramString, localObject);
    }
    ((List)localObject).add(paramException);
    chronologicalErrorMessages.add(paramException.getMessage());
  }
  
  public Object getObject(String paramString)
  {
    Object localObject = null;
    List localList = (List)allResults.get(paramString);
    if ((localList != null) && (localList.size() > 0)) {
      localObject = localList.get(0);
    }
    return localObject;
  }
  
  public Object[] getObjectArray(String paramString)
  {
    Object localObject = (List)allResults.get(paramString);
    if (localObject == null) {
      localObject = new ArrayList(0);
    }
    return ((List)localObject).toArray();
  }
  
  public Object[] getObjectArray(String paramString, Object[] paramArrayOfObject)
  {
    Object localObject = (List)allResults.get(paramString);
    if (localObject == null) {
      localObject = new ArrayList();
    }
    return ((List)localObject).toArray(paramArrayOfObject);
  }
  
  public boolean getBoolean(String paramString)
  {
    Boolean localBoolean = (Boolean)qualifiedSwitches.get(paramString);
    if (localBoolean == null)
    {
      if (!contains(paramString)) {
        throw new UnspecifiedParameterException(paramString);
      }
      localBoolean = (Boolean)getObject(paramString);
    }
    return localBoolean.booleanValue();
  }
  
  public boolean getBoolean(String paramString, boolean paramBoolean)
  {
    Boolean localBoolean = (Boolean)qualifiedSwitches.get(paramString);
    if (localBoolean == null) {
      localBoolean = (Boolean)getObject(paramString);
    }
    return localBoolean == null ? paramBoolean : localBoolean.booleanValue();
  }
  
  public boolean[] getBooleanArray(String paramString)
  {
    Boolean[] arrayOfBoolean = (Boolean[])getObjectArray(paramString, new Boolean[0]);
    int i = arrayOfBoolean.length;
    boolean[] arrayOfBoolean1 = new boolean[i];
    for (int j = 0; j < i; j++) {
      arrayOfBoolean1[j] = arrayOfBoolean[j].booleanValue();
    }
    return arrayOfBoolean1;
  }
  
  public int getInt(String paramString)
  {
    if (!contains(paramString)) {
      throw new UnspecifiedParameterException(paramString);
    }
    return ((Integer)getObject(paramString)).intValue();
  }
  
  public int getInt(String paramString, int paramInt)
  {
    Integer localInteger = (Integer)getObject(paramString);
    return localInteger == null ? paramInt : localInteger.intValue();
  }
  
  public String getQualifiedSwitchValue(String paramString)
  {
    Object localObject = null;
    List localList = (List)allResults.get(paramString);
    if ((localList != null) && (localList.size() == 2)) {
      localObject = localList.get(1);
    }
    return (String)localObject;
  }
  
  public int[] getIntArray(String paramString)
  {
    Integer[] arrayOfInteger = (Integer[])getObjectArray(paramString, new Integer[0]);
    int i = arrayOfInteger.length;
    int[] arrayOfInt = new int[i];
    for (int j = 0; j < i; j++) {
      arrayOfInt[j] = arrayOfInteger[j].intValue();
    }
    return arrayOfInt;
  }
  
  public long getLong(String paramString)
  {
    if (!contains(paramString)) {
      throw new UnspecifiedParameterException(paramString);
    }
    return ((Long)getObject(paramString)).longValue();
  }
  
  public long getLong(String paramString, long paramLong)
  {
    Long localLong = (Long)getObject(paramString);
    return localLong == null ? paramLong : localLong.longValue();
  }
  
  public long[] getLongArray(String paramString)
  {
    Long[] arrayOfLong = (Long[])getObjectArray(paramString, new Long[0]);
    int i = arrayOfLong.length;
    long[] arrayOfLong1 = new long[i];
    for (int j = 0; j < i; j++) {
      arrayOfLong1[j] = arrayOfLong[j].longValue();
    }
    return arrayOfLong1;
  }
  
  public byte getByte(String paramString)
  {
    if (!contains(paramString)) {
      throw new UnspecifiedParameterException(paramString);
    }
    return ((Byte)getObject(paramString)).byteValue();
  }
  
  public byte getByte(String paramString, byte paramByte)
  {
    Byte localByte = (Byte)getObject(paramString);
    return localByte == null ? paramByte : localByte.byteValue();
  }
  
  public byte[] getByteArray(String paramString)
  {
    Byte[] arrayOfByte = (Byte[])getObjectArray(paramString, new Byte[0]);
    int i = arrayOfByte.length;
    byte[] arrayOfByte1 = new byte[i];
    for (int j = 0; j < i; j++) {
      arrayOfByte1[j] = arrayOfByte[j].byteValue();
    }
    return arrayOfByte1;
  }
  
  public char getChar(String paramString)
  {
    if (!contains(paramString)) {
      throw new UnspecifiedParameterException(paramString);
    }
    return ((Character)getObject(paramString)).charValue();
  }
  
  public char getChar(String paramString, char paramChar)
  {
    Character localCharacter = (Character)getObject(paramString);
    return localCharacter == null ? paramChar : localCharacter.charValue();
  }
  
  public char[] getCharArray(String paramString)
  {
    Character[] arrayOfCharacter = (Character[])getObjectArray(paramString, new Character[0]);
    int i = arrayOfCharacter.length;
    char[] arrayOfChar = new char[i];
    for (int j = 0; j < i; j++) {
      arrayOfChar[j] = arrayOfCharacter[j].charValue();
    }
    return arrayOfChar;
  }
  
  public short getShort(String paramString)
  {
    if (!contains(paramString)) {
      throw new UnspecifiedParameterException(paramString);
    }
    return ((Short)getObject(paramString)).shortValue();
  }
  
  public short getShort(String paramString, short paramShort)
  {
    Short localShort = (Short)getObject(paramString);
    return localShort == null ? paramShort : localShort.shortValue();
  }
  
  public short[] getShortArray(String paramString)
  {
    Short[] arrayOfShort = (Short[])getObjectArray(paramString, new Short[0]);
    int i = arrayOfShort.length;
    short[] arrayOfShort1 = new short[i];
    for (int j = 0; j < i; j++) {
      arrayOfShort1[j] = arrayOfShort[j].shortValue();
    }
    return arrayOfShort1;
  }
  
  public double getDouble(String paramString)
  {
    if (!contains(paramString)) {
      throw new UnspecifiedParameterException(paramString);
    }
    return ((Double)getObject(paramString)).doubleValue();
  }
  
  public double getDouble(String paramString, double paramDouble)
  {
    Double localDouble = (Double)getObject(paramString);
    return localDouble == null ? paramDouble : localDouble.doubleValue();
  }
  
  public double[] getDoubleArray(String paramString)
  {
    Double[] arrayOfDouble = (Double[])getObjectArray(paramString, new Double[0]);
    int i = arrayOfDouble.length;
    double[] arrayOfDouble1 = new double[i];
    for (int j = 0; j < i; j++) {
      arrayOfDouble1[j] = arrayOfDouble[j].doubleValue();
    }
    return arrayOfDouble1;
  }
  
  public float getFloat(String paramString)
  {
    if (!contains(paramString)) {
      throw new UnspecifiedParameterException(paramString);
    }
    return ((Float)getObject(paramString)).floatValue();
  }
  
  public float getFloat(String paramString, float paramFloat)
  {
    Float localFloat = (Float)getObject(paramString);
    return localFloat == null ? paramFloat : localFloat.floatValue();
  }
  
  public float[] getFloatArray(String paramString)
  {
    Float[] arrayOfFloat = (Float[])getObjectArray(paramString, new Float[0]);
    int i = arrayOfFloat.length;
    float[] arrayOfFloat1 = new float[i];
    for (int j = 0; j < i; j++) {
      arrayOfFloat1[j] = arrayOfFloat[j].floatValue();
    }
    return arrayOfFloat1;
  }
  
  public String getString(String paramString)
  {
    return (String)getObject(paramString);
  }
  
  public String getString(String paramString1, String paramString2)
  {
    String str = (String)getObject(paramString1);
    return str == null ? paramString2 : str;
  }
  
  public String[] getStringArray(String paramString)
  {
    return (String[])getObjectArray(paramString, new String[0]);
  }
  
  public BigDecimal getBigDecimal(String paramString)
  {
    return (BigDecimal)getObject(paramString);
  }
  
  public BigDecimal getBigDecimal(String paramString, BigDecimal paramBigDecimal)
  {
    BigDecimal localBigDecimal = (BigDecimal)getObject(paramString);
    return localBigDecimal == null ? paramBigDecimal : localBigDecimal;
  }
  
  public BigDecimal[] getBigDecimalArray(String paramString)
  {
    return (BigDecimal[])getObjectArray(paramString, new BigDecimal[0]);
  }
  
  public BigInteger getBigInteger(String paramString)
  {
    return (BigInteger)getObject(paramString);
  }
  
  public BigInteger getBigInteger(String paramString, BigInteger paramBigInteger)
  {
    BigInteger localBigInteger = (BigInteger)getObject(paramString);
    return localBigInteger == null ? paramBigInteger : localBigInteger;
  }
  
  public BigInteger[] getBigIntegerArray(String paramString)
  {
    return (BigInteger[])getObjectArray(paramString, new BigInteger[0]);
  }
  
  public Class getClass(String paramString)
  {
    return (Class)getObject(paramString);
  }
  
  public Class getClass(String paramString, Class paramClass)
  {
    Class localClass = (Class)getObject(paramString);
    return localClass == null ? paramClass : localClass;
  }
  
  public Class[] getClassArray(String paramString)
  {
    return (Class[])getObjectArray(paramString, new Class[0]);
  }
  
  public InetAddress getInetAddress(String paramString)
  {
    return (InetAddress)getObject(paramString);
  }
  
  public InetAddress getInetAddress(String paramString, InetAddress paramInetAddress)
  {
    InetAddress localInetAddress = (InetAddress)getObject(paramString);
    return localInetAddress == null ? paramInetAddress : localInetAddress;
  }
  
  public InetAddress[] getInetAddressArray(String paramString)
  {
    return (InetAddress[])getObjectArray(paramString, new InetAddress[0]);
  }
  
  public Package getPackage(String paramString)
  {
    return (Package)getObject(paramString);
  }
  
  public Package getPackage(String paramString, Package paramPackage)
  {
    Package localPackage = (Package)getObject(paramString);
    return localPackage == null ? paramPackage : localPackage;
  }
  
  public Package[] getPackageArray(String paramString)
  {
    return (Package[])getObjectArray(paramString, new Package[0]);
  }
  
  public URL getURL(String paramString)
  {
    return (URL)getObject(paramString);
  }
  
  public URL getURL(String paramString, URL paramURL)
  {
    URL localURL = (URL)getObject(paramString);
    return localURL == null ? paramURL : localURL;
  }
  
  public URL[] getURLArray(String paramString)
  {
    return (URL[])getObjectArray(paramString, new URL[0]);
  }
  
  public Color getColor(String paramString)
  {
    return (Color)getObject(paramString);
  }
  
  public File getFile(String paramString)
  {
    return (File)getObject(paramString);
  }
  
  public Color getColor(String paramString, Color paramColor)
  {
    Color localColor = (Color)getObject(paramString);
    return localColor == null ? paramColor : localColor;
  }
  
  public File getFile(String paramString, File paramFile)
  {
    File localFile = (File)getObject(paramString);
    return localFile == null ? paramFile : localFile;
  }
  
  public Color[] getColorArray(String paramString)
  {
    return (Color[])getObjectArray(paramString, new Color[0]);
  }
  
  public File[] getFileArray(String paramString)
  {
    return (File[])getObjectArray(paramString, new File[0]);
  }
  
  public Date getDate(String paramString)
  {
    return (Date)getObject(paramString);
  }
  
  public Date getDate(String paramString, Date paramDate)
  {
    Date localDate = (Date)getObject(paramString);
    return localDate == null ? paramDate : localDate;
  }
  
  public Date[] getDateArray(String paramString)
  {
    return (Date[])getObjectArray(paramString, new Date[0]);
  }
  
  public Exception getException(String paramString)
  {
    Exception localException = null;
    List localList = (List)allExceptions.get(paramString);
    if ((localList != null) && (localList.size() > 0)) {
      localException = (Exception)localList.get(0);
    }
    return localException;
  }
  
  public Exception[] getExceptionArray(String paramString)
  {
    Exception[] arrayOfException = new Exception[0];
    List localList = (List)allExceptions.get(paramString);
    if (localList != null) {
      arrayOfException = (Exception[])localList.toArray(arrayOfException);
    }
    return arrayOfException;
  }
  
  public Iterator getExceptionIterator(String paramString)
  {
    Object localObject = (List)allExceptions.get(paramString);
    if (localObject == null) {
      localObject = new ArrayList();
    }
    return ((List)localObject).iterator();
  }
  
  public Iterator getErrorMessageIterator()
  {
    return chronologicalErrorMessages.iterator();
  }
  
  public Iterator getBadParameterIDIterator()
  {
    return allExceptions.keySet().iterator();
  }
  
  public boolean success()
  {
    return allExceptions.size() == 0;
  }
  
  void registerQualifiedSwitch(String paramString, boolean paramBoolean)
  {
    qualifiedSwitches.put(paramString, new Boolean(paramBoolean));
  }
}
