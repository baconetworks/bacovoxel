package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class IntegerStringParser
  extends StringParser
{
  private static final IntegerStringParser INSTANCE = new IntegerStringParser();
  
  public static IntegerStringParser getParser()
  {
    return new IntegerStringParser();
  }
  
  /**
   * @deprecated
   */
  public IntegerStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    Integer localInteger = null;
    try
    {
      localInteger = Integer.decode(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to an Integer.", localNumberFormatException);
    }
    return localInteger;
  }
}
