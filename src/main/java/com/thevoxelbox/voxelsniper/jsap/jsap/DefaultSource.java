package com.thevoxelbox.voxelsniper.jsap.jsap;

public abstract interface DefaultSource
{
  public abstract Defaults getDefaults(IDMap paramIDMap, ExceptionMap paramExceptionMap);
}
