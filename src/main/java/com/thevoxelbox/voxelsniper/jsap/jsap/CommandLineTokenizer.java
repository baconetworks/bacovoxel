package com.thevoxelbox.voxelsniper.jsap.jsap;

import java.util.ArrayList;
import java.util.List;

public class CommandLineTokenizer
{
  private CommandLineTokenizer() {}
  
  private static void appendToBuffer(List paramList, StringBuffer paramStringBuffer)
  {
    if (paramStringBuffer.length() > 0)
    {
      paramList.add(paramStringBuffer.toString());
      paramStringBuffer.setLength(0);
    }
  }
  
  public static String[] tokenize(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    if (paramString != null)
    {
      int i = paramString.length();
      int j = 0;
      StringBuffer localStringBuffer = new StringBuffer();
      for (int k = 0; k < i; k++)
      {
        char c = paramString.charAt(k);
        if (c == '"')
        {
          appendToBuffer(localArrayList, localStringBuffer);
          j = j == 0 ? 1 : 0;
        }
        else if (c == '\\')
        {
          if ((i > k + 1) && ((paramString.charAt(k + 1) == '"') || (paramString.charAt(k + 1) == '\\')))
          {
            localStringBuffer.append(paramString.charAt(k + 1));
            k++;
          }
          else
          {
            localStringBuffer.append("\\");
          }
        }
        else if (j != 0)
        {
          localStringBuffer.append(c);
        }
        else if (Character.isWhitespace(c))
        {
          appendToBuffer(localArrayList, localStringBuffer);
        }
        else
        {
          localStringBuffer.append(c);
        }
      }
      appendToBuffer(localArrayList, localStringBuffer);
    }
    String[] arrayOfString = new String[localArrayList.size()];
    return (String[])localArrayList.toArray(arrayOfString);
  }
}
