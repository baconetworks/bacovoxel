package com.thevoxelbox.voxelsniper.jsap.jsap;

public class ParseException
  extends JSAPException
{
  public ParseException() {}
  
  public ParseException(String paramString)
  {
    super(paramString);
  }
  
  public ParseException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
  
  public ParseException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}
