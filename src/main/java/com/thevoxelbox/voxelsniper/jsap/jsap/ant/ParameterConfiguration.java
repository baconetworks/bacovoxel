package com.thevoxelbox.voxelsniper.jsap.jsap.ant;

import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

public abstract class ParameterConfiguration
{
  private String id = null;
  private Vector defaults = null;
  
  public ParameterConfiguration() {}
  
  public abstract Parameter getParameter();
  
  public void setId(String paramString)
  {
    id = paramString;
  }
  
  public String getId()
  {
    return id;
  }
  
  public void setDefault(String paramString)
  {
    defaults.add(paramString);
  }
  
  public void addConfiguredDefault(DefaultValue paramDefaultValue)
  {
    defaults.add(paramDefaultValue.getValue());
  }
  
  public String[] getDefaults()
  {
    return defaults.size() == 0 ? null : (String[])defaults.toArray(new String[0]);
  }
  
  protected void createParentStatements(String paramString, PrintStream paramPrintStream)
  {
    String[] arrayOfString = getDefaults();
    if (arrayOfString != null) {
      for (int i = 0; i < arrayOfString.length; i++) {
        paramPrintStream.println("        " + paramString + ".addDefault(\"" + arrayOfString[i] + "\");");
      }
    }
  }
  
  public boolean hasProperties()
  {
    return false;
  }
  
  public abstract void createMethod(String paramString, PrintStream paramPrintStream)
    throws IOException;
}
