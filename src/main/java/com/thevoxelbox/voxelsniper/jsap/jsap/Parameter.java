package com.thevoxelbox.voxelsniper.jsap.jsap;

import java.util.List;

public abstract class Parameter
{
  private String id = null;
  private String usageName = null;
  private boolean locked = false;
  private String[] defaultValue = null;
  private String help = null;
  
  public Parameter(String paramString)
  {
    id = paramString;
  }
  
  public String getID()
  {
    return id;
  }
  
  protected final void setLocked(boolean paramBoolean)
  {
    locked = paramBoolean;
  }
  
  protected final boolean locked()
  {
    return locked;
  }
  
  protected final void enforceParameterLock()
  {
    if (locked) {
      throw new IllegalStateException("Parameter '" + getID() + "' may not be changed.");
    }
  }
  
  protected final void _setDefault(String paramString)
  {
    if (paramString == JSAP.NO_DEFAULT)
    {
      defaultValue = null;
    }
    else
    {
      defaultValue = new String[1];
      defaultValue[0] = paramString;
    }
  }
  
  protected final void _setDefault(String[] paramArrayOfString)
  {
    defaultValue = paramArrayOfString;
  }
  
  public final void addDefault(String paramString)
  {
    if (paramString != JSAP.NO_DEFAULT)
    {
      if (defaultValue == null) {
        defaultValue = new String[0];
      }
      int i = defaultValue.length + 1;
      String[] arrayOfString = new String[i];
      for (int j = 0; j < i - 1; j++) {
        arrayOfString[j] = defaultValue[j];
      }
      arrayOfString[(i - 1)] = paramString;
      defaultValue = arrayOfString;
    }
  }
  
  protected final void _setUsageName(String paramString)
  {
    usageName = paramString;
  }
  
  public final String getUsageName()
  {
    return usageName == null ? id : usageName;
  }
  
  public final String[] getDefault()
  {
    return defaultValue;
  }
  
  protected abstract List parse(String paramString)
    throws ParseException;
  
  public abstract String getSyntax();
  
  /**
   * @deprecated
   */
  public final String getUsage()
  {
    return getSyntax();
  }
  
  public final String getHelp()
  {
    return help == null ? "" : help;
  }
  
  public final Parameter setHelp(String paramString)
  {
    help = paramString;
    return this;
  }
}
