package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import com.thevoxelbox.voxelsniper.jsap.jsap.JSAPException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JSAPConfig
{
  private List parameters = new ArrayList();
  private String help = null;
  private String usage = null;
  
  public JSAPConfig() {}
  
  public static void configure(JSAP paramJSAP, URL paramURL)
    throws IOException, JSAPException
  {
    JSAPXStream localJSAPXStream = new JSAPXStream();
    InputStreamReader localInputStreamReader = new InputStreamReader(paramURL.openStream());
    JSAPConfig localJSAPConfig = (JSAPConfig)localJSAPXStream.fromXML(localInputStreamReader);
    localInputStreamReader.close();
    Iterator localIterator = localJSAPConfig.parameters();
    while (localIterator.hasNext())
    {
      ParameterConfig localParameterConfig = (ParameterConfig)localIterator.next();
      paramJSAP.registerParameter(localParameterConfig.getConfiguredParameter());
    }
    paramJSAP.setHelp(localJSAPConfig.getHelp());
    paramJSAP.setUsage(localJSAPConfig.getUsage());
  }
  
  public String getHelp()
  {
    return help;
  }
  
  public void setHelp(String paramString)
  {
    help = paramString;
  }
  
  public String getUsage()
  {
    return usage;
  }
  
  public void setUsage(String paramString)
  {
    usage = paramString;
  }
  
  public void addParameter(ParameterConfig paramParameterConfig)
  {
    parameters.add(paramParameterConfig);
  }
  
  public Iterator parameters()
  {
    return parameters.iterator();
  }
}
