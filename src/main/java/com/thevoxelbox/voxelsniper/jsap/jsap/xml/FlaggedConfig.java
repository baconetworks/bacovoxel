package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.FlaggedOption;
import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import com.thevoxelbox.voxelsniper.jsap.jsap.QualifiedSwitch;

abstract class FlaggedConfig
  extends ParameterConfig
{
  private char shortFlag = '\000';
  private String longFlag = JSAP.NO_LONGFLAG;
  
  FlaggedConfig() {}
  
  public String getLongFlag()
  {
    return longFlag;
  }
  
  public void setLongFlag(String paramString)
  {
    longFlag = paramString;
  }
  
  public char getShortFlag()
  {
    return shortFlag;
  }
  
  public void setShortFlag(char paramChar)
  {
    shortFlag = paramChar;
  }
  
  protected void configure(QualifiedSwitch paramFlaggedOption)
  {
    super.configure(paramFlaggedOption);
  }
}
