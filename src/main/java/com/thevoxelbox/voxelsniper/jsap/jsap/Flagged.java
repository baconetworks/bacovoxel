package com.thevoxelbox.voxelsniper.jsap.jsap;

public abstract interface Flagged
{
  public abstract char getShortFlag();
  
  public abstract Character getShortFlagCharacter();
  
  public abstract String getLongFlag();
}
