package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class ShortStringParser
  extends StringParser
{
  private static final ShortStringParser INSTANCE = new ShortStringParser();
  
  public static ShortStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public ShortStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    Short localShort = null;
    try
    {
      localShort = Short.decode(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to a Short.", localNumberFormatException);
    }
    return localShort;
  }
}
