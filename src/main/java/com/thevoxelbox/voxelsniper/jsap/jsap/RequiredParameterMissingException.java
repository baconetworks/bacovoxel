package com.thevoxelbox.voxelsniper.jsap.jsap;

public class RequiredParameterMissingException
  extends JSAPException
{
  private String id = null;
  
  public RequiredParameterMissingException(String paramString)
  {
    super("Parameter '" + paramString + "' is required.");
    id = paramString;
  }
  
  public String getID()
  {
    return id;
  }
}
