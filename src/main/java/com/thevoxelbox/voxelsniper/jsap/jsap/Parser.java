package com.thevoxelbox.voxelsniper.jsap.jsap;

import java.util.Iterator;

class Parser
{
  private JSAP config = null;
  private Iterator unflaggedOptions = null;
  private UnflaggedOption curUnflaggedOption = null;
  private String[] args = null;
  private boolean parsed = false;
  private JSAPResult result = null;
  
  public Parser(JSAP paramJSAP, String[] paramArrayOfString)
  {
    config = paramJSAP;
    args = paramArrayOfString;
    parsed = false;
    result = new JSAPResult();
    unflaggedOptions = paramJSAP.getUnflaggedOptionsIterator();
    advanceUnflaggedOption();
  }
  
  private void advanceUnflaggedOption()
  {
    if (unflaggedOptions.hasNext()) {
      curUnflaggedOption = ((UnflaggedOption)unflaggedOptions.next());
    } else {
      curUnflaggedOption = null;
    }
  }
  
  private void processParameter(Parameter paramParameter, String paramString)
  {
    if ((result.getObject(paramParameter.getID()) != null) && ((((paramParameter instanceof FlaggedOption)) && (!((FlaggedOption)paramParameter).allowMultipleDeclarations())) || ((paramParameter instanceof Switch)) || ((paramParameter instanceof QualifiedSwitch)) || (((paramParameter instanceof UnflaggedOption)) && (!((UnflaggedOption)paramParameter).isGreedy())))) {
      result.addException(paramParameter.getID(), new IllegalMultipleDeclarationException(paramParameter.getID()));
    }
    if ((paramParameter instanceof QualifiedSwitch)) {
      result.registerQualifiedSwitch(paramParameter.getID(), true);
    }
    try
    {
      result.add(paramParameter.getID(), paramParameter.parse(paramString));
    }
    catch (ParseException localParseException)
    {
      result.addException(paramParameter.getID(), localParseException);
    }
  }
  
  private int parseArg(String[] paramArrayOfString, int paramInt)
  {
    if ((paramArrayOfString[paramInt].startsWith("--")) && (!paramArrayOfString[paramInt].equals("--"))) {
      return parseLongForm(paramArrayOfString, paramInt);
    }
    if ((paramArrayOfString[paramInt].startsWith("-")) && (!paramArrayOfString[paramInt].equals("-")) && (!paramArrayOfString[paramInt].equals("--"))) {
      return parseShortForm(paramArrayOfString, paramInt);
    }
    return parseUnflaggedOption(paramArrayOfString, paramInt);
  }
  
  private void processDefaults()
  {
    Defaults localDefaults = config.getDefaults(result);
    Iterator localIterator = localDefaults.idIterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (result.getObject(str) == null)
      {
        String[] arrayOfString = localDefaults.getDefault(str);
        if (arrayOfString != null)
        {
          int i = arrayOfString.length;
          for (int j = 0; j < i; j++) {
            processParameter(config.getByID(str), arrayOfString[j]);
          }
        }
      }
    }
  }
  
  private void enforceRequirements()
  {
    IDMap localIDMap = config.getIDMap();
    Iterator localIterator = localIDMap.idIterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Parameter localParameter = config.getByID(str);
      if ((localParameter instanceof Option))
      {
        Option localOption = (Option)localParameter;
        if ((localOption.required()) && (result.getObject(str) == null) && (result.getException(str) == null)) {
          result.addException(localOption.getID(), new RequiredParameterMissingException(str));
        }
      }
    }
  }
  
  public JSAPResult parse()
  {
    if (parsed)
    {
      result.addException(null, new JSAPException("This Parser has already run."));
    }
    else
    {
      result.setValuesFromUser(true);
      preregisterQualifiedSwitches();
      int i = args.length;
      for (int j = 0; j < i; j = parseArg(args, j)) {}
      result.setValuesFromUser(false);
      processDefaults();
      enforceRequirements();
      parsed = true;
    }
    return result;
  }
  
  private void preregisterQualifiedSwitches()
  {
    Iterator localIterator = config.getIDMap().idIterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Parameter localParameter = config.getByID(str);
      if ((localParameter instanceof QualifiedSwitch)) {
        result.registerQualifiedSwitch(str, false);
      }
    }
  }
  
  private int parseLongForm(String[] paramArrayOfString, int paramInt)
  {
    int i = paramArrayOfString[paramInt].indexOf('=');
    int j = paramArrayOfString[paramInt].indexOf(':');
    String str1 = null;
    String str2 = null;
    String str3 = null;
    if ((i == -1) && (j == -1)) {
      str1 = paramArrayOfString[paramInt].substring(2);
    }
    if (i != -1)
    {
      str1 = paramArrayOfString[paramInt].substring(2, i);
      str2 = paramArrayOfString[paramInt].substring(i + 1);
    }
    if ((i == -1) && (j != -1))
    {
      str1 = paramArrayOfString[paramInt].substring(2, j);
      str3 = paramArrayOfString[paramInt].substring(j);
    }
    paramInt++;
    Flagged localFlagged = config.getByLongFlag(str1);
    Parameter localParameter = (Parameter)localFlagged;
    if (localFlagged == null)
    {
      result.addException(null, new UnknownFlagException(str1));
    }
    else if ((localFlagged instanceof Switch))
    {
      if (i != -1) {
        result.addException(localParameter.getID(), new SyntaxException("Switch \"" + str1 + "\" does not take any parameters."));
      } else {
        processParameter(localParameter, null);
      }
    }
    else if ((localParameter instanceof QualifiedSwitch))
    {
      Object localObject = null;
      if (j == -1) {
        processParameter(localParameter, null);
      } else {
        processParameter(localParameter, paramArrayOfString[(paramInt - 1)].substring(j + 1));
      }
    }
    else if (i == -1)
    {
      if (paramInt >= paramArrayOfString.length)
      {
        result.addException(localParameter.getID(), new SyntaxException("No value specified for option \"" + str1 + "\""));
      }
      else
      {
        str2 = paramArrayOfString[paramInt];
        paramInt++;
        processParameter(localParameter, str2);
      }
    }
    else
    {
      processParameter(localParameter, str2);
    }
    return paramInt;
  }
  
  private int parseShortForm(String[] paramArrayOfString, int paramInt)
  {
    Character localCharacter = null;
    int i = paramArrayOfString[paramInt].indexOf('=');
    int j = paramArrayOfString[paramInt].length();
    if (i != -1) {
      j = i;
    }
    for (int k = 1; k < j; k++)
    {
      localCharacter = new Character(paramArrayOfString[paramInt].charAt(k));
      Flagged localFlagged = config.getByShortFlag(localCharacter);
      Parameter localParameter = (Parameter)localFlagged;
      if (localFlagged == null)
      {
        result.addException(null, new UnknownFlagException(localCharacter));
      }
      else if ((localFlagged instanceof Switch))
      {
        if (k == i - 1) {
          result.addException(localParameter.getID(), new SyntaxException("Switch \"" + localCharacter + "\" does not take any parameters."));
        } else {
          processParameter(localParameter, null);
        }
      }
      else if ((localFlagged instanceof QualifiedSwitch))
      {
        if ((paramArrayOfString[paramInt].length() > k + 1) && (paramArrayOfString[paramInt].charAt(k + 1) == ':'))
        {
          processParameter(localParameter, paramArrayOfString[paramInt].substring(k + 2));
          break;
        }
        processParameter(localParameter, null);
      }
      else
      {
        String str = null;
        if (i != -1)
        {
          str = paramArrayOfString[paramInt].substring(i + 1);
          processParameter(localParameter, str);
        }
        else
        {
          if (k < j - 1)
          {
            str = paramArrayOfString[paramInt].substring(k + 1, j);
            processParameter(localParameter, str);
            break;
          }
          paramInt++;
          if (paramInt >= paramArrayOfString.length)
          {
            result.addException(localParameter.getID(), new SyntaxException("No value specified for option \"" + localCharacter + "\"."));
          }
          else
          {
            str = paramArrayOfString[paramInt];
            processParameter(localParameter, str);
          }
        }
      }
    }
    paramInt++;
    return paramInt;
  }
  
  private int parseUnflaggedOption(String[] paramArrayOfString, int paramInt)
  {
    if (curUnflaggedOption != null)
    {
      processParameter(curUnflaggedOption, paramArrayOfString[paramInt]);
      if (!curUnflaggedOption.isGreedy()) {
        advanceUnflaggedOption();
      }
    }
    else
    {
      result.addException(null, new JSAPException("Unexpected argument: " + paramArrayOfString[paramInt]));
    }
    return paramInt + 1;
  }
}
