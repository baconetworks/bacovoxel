package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import com.thevoxelbox.voxelsniper.jsap.jsap.QualifiedSwitch;

class QualifiedSwitchConfig
  extends FlaggedOptionConfig
{
  QualifiedSwitchConfig() {}
  
  protected void configure(QualifiedSwitch paramQualifiedSwitch)
  {
    super.configure(paramQualifiedSwitch);
  }
  
  public Parameter getConfiguredParameter()
  {
    QualifiedSwitch localQualifiedSwitch = new QualifiedSwitch(getId());
    configure(localQualifiedSwitch);
    return localQualifiedSwitch;
  }
}
