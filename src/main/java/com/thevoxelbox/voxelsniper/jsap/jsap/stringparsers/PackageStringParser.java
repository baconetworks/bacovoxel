package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class PackageStringParser
  extends StringParser
{
  private static final PackageStringParser INSTANCE = new PackageStringParser();
  
  public static PackageStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public PackageStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    Package localPackage = Package.getPackage(paramString);
    if (localPackage == null) {
      throw new ParseException("Unable to locate Package '" + paramString + "'.");
    }
    return localPackage;
  }
}
