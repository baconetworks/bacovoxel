package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import java.io.PrintStream;

public class TryLoadXML
{
  public TryLoadXML() {}
  
  public static void main(String[] paramArrayOfString)
    throws Exception
  {
    JSAP localJSAP = new JSAP("com/martiansoftware/jsap/xml/silly-example.xml");
    System.out.println(localJSAP.getHelp());
  }
}
