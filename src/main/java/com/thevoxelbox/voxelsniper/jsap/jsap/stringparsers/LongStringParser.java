package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class LongStringParser
  extends StringParser
{
  private static final LongStringParser INSTANCE = new LongStringParser();
  
  public static LongStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public LongStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    Long localLong = null;
    try
    {
      localLong = Long.decode(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to a Long.", localNumberFormatException);
    }
    return localLong;
  }
}
