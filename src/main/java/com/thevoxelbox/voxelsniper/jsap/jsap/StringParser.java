package com.thevoxelbox.voxelsniper.jsap.jsap;

public abstract class StringParser
{
  public StringParser() {}
  
  public void setUp()
    throws Exception
  {}
  
  public void tearDown() {}
  
  public abstract Object parse(String paramString)
    throws ParseException;
}
