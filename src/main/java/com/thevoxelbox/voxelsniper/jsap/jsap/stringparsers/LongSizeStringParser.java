package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LongSizeStringParser
  extends StringParser
{
  private static final Pattern PARSE_SIZE_REGEX = Pattern.compile("((#|0x|0X)?[0-9A-Fa-f]+)([KMGTP]i?)?");
  private static final BigInteger LONG_MAX_VALUE = new BigInteger(Long.toString(Long.MAX_VALUE));
  private static final HashMap UNIT2SIZE = new HashMap();
  static final LongSizeStringParser INSTANCE = new LongSizeStringParser();
  
  private LongSizeStringParser() {}
  
  public static LongSizeStringParser getParser()
  {
    return INSTANCE;
  }
  
  public Object parse(String paramString)
    throws ParseException
  {
    return new Long(parseSize(paramString));
  }
  
  public static long parseSize(CharSequence paramCharSequence)
    throws ParseException
  {
    Matcher localMatcher = PARSE_SIZE_REGEX.matcher(paramCharSequence);
    if (!localMatcher.matches()) {
      throw new ParseException("Invalid size specification '" + paramCharSequence + "'.");
    }
    String str = localMatcher.group(3);
    BigInteger localBigInteger1 = BigInteger.ONE;
    if (str != null)
    {
      Long localObject = (Long) UNIT2SIZE.get(str);
      if (localObject == null) {
        throw new ParseException("Invalid unit specification '" + str + "'.");
      }
      localBigInteger1 = new BigInteger(((Long)localObject).toString());
    }
    Object localObject = localMatcher.group(1);
    Long localLong;
    try
    {
      localLong = Long.decode((String)localObject);
      if (localLong.longValue() < 0L) {
        throw new ParseException("Sizes cannot be negative.");
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Invalid number '" + (String)localObject + "'.");
    }
    BigInteger localBigInteger2 = new BigInteger(localLong.toString()).multiply(localBigInteger1);
    if (localBigInteger2.compareTo(LONG_MAX_VALUE) > 0) {
      throw new ParseException("Size '" + paramCharSequence + "' is too big.");
    }
    return Long.parseLong(localBigInteger2.toString());
  }
  
  static
  {
    UNIT2SIZE.put("K", new Long(1000L));
    UNIT2SIZE.put("M", new Long(1000000L));
    UNIT2SIZE.put("G", new Long(1000000000L));
    UNIT2SIZE.put("T", new Long(1000000000000L));
    UNIT2SIZE.put("P", new Long(1000000000000000L));
    UNIT2SIZE.put("Ki", new Long(1024L));
    UNIT2SIZE.put("Mi", new Long(1048576L));
    UNIT2SIZE.put("Gi", new Long(1073741824L));
    UNIT2SIZE.put("Ti", new Long(1099511627776L));
    UNIT2SIZE.put("Pi", new Long(1125899906842624L));
  }
}
