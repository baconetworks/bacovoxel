package com.thevoxelbox.voxelsniper.jsap.jsap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Defaults
{
  private HashMap defaults = null;
  
  public Defaults() {}
  
  public void setDefault(String paramString1, String paramString2)
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString2;
    setDefault(paramString1, arrayOfString);
  }
  
  public void setDefault(String paramString, String[] paramArrayOfString)
  {
    defaults.put(paramString, paramArrayOfString);
  }
  
  public void addDefault(String paramString1, String paramString2)
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString2;
    addDefault(paramString1, arrayOfString);
  }
  
  public void addDefault(String paramString, String[] paramArrayOfString)
  {
    String[] arrayOfString1 = getDefault(paramString);
    if (arrayOfString1 == null) {
      arrayOfString1 = new String[0];
    }
    String[] arrayOfString2 = new String[arrayOfString1.length + paramArrayOfString.length];
    int i = 0;
    for (int j = 0; j < arrayOfString1.length; j++)
    {
      arrayOfString2[i] = arrayOfString1[j];
      i++;
    }
    int j;
    for (j = 0; j < paramArrayOfString.length; j++)
    {
      arrayOfString2[i] = paramArrayOfString[j];
      i++;
    }
    setDefault(paramString, arrayOfString2);
  }
  
  protected void setDefaultIfNeeded(String paramString1, String paramString2)
  {
    if (!defaults.containsKey(paramString1)) {
      setDefault(paramString1, paramString2);
    }
  }
  
  protected void setDefaultIfNeeded(String paramString, String[] paramArrayOfString)
  {
    if (!defaults.containsKey(paramString)) {
      setDefault(paramString, paramArrayOfString);
    }
  }
  
  public String[] getDefault(String paramString)
  {
    return (String[])defaults.get(paramString);
  }
  
  public Iterator idIterator()
  {
    return defaults.keySet().iterator();
  }
}
