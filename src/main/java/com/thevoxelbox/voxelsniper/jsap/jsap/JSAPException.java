package com.thevoxelbox.voxelsniper.jsap.jsap;

public class JSAPException
  extends Exception
{
  public JSAPException() {}
  
  public JSAPException(String paramString)
  {
    super(paramString);
  }
  
  public JSAPException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
  
  public JSAPException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}
