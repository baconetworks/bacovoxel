package com.thevoxelbox.voxelsniper.jsap.jsap;

public class UnspecifiedParameterException
  extends RuntimeException
{
  private String id = null;
  
  public UnspecifiedParameterException(String paramString)
  {
    super("Parameter '" + paramString + "' has no associated value.");
    id = paramString;
  }
  
  public String getID()
  {
    return id;
  }
}
