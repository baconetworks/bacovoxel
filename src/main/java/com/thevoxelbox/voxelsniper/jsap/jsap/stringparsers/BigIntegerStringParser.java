package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.math.BigInteger;

public class BigIntegerStringParser
  extends StringParser
{
  private static final BigIntegerStringParser INSTANCE = new BigIntegerStringParser();
  
  public static BigIntegerStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public BigIntegerStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    BigInteger localBigInteger = null;
    try
    {
      localBigInteger = new BigInteger(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to a BigInteger.", localNumberFormatException);
    }
    return localBigInteger;
  }
}
