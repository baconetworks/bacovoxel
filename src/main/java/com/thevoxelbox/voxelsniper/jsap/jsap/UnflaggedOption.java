package com.thevoxelbox.voxelsniper.jsap.jsap;

public final class UnflaggedOption
  extends Option
{
  private boolean greedy = false;
  
  public UnflaggedOption(String paramString)
  {
    super(paramString);
  }
  
  public UnflaggedOption(String paramString1, StringParser paramStringParser, String paramString2, boolean paramBoolean1, boolean paramBoolean2, String paramString3)
  {
    this(paramString1);
    setStringParser(paramStringParser);
    setDefault(paramString2);
    setRequired(paramBoolean1);
    setGreedy(paramBoolean2);
    setHelp(paramString3);
  }
  
  public UnflaggedOption(String paramString1, StringParser paramStringParser, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    this(paramString1);
    setStringParser(paramStringParser);
    setDefault(paramString2);
    setRequired(paramBoolean1);
    setGreedy(paramBoolean2);
  }
  
  public UnflaggedOption(String paramString1, StringParser paramStringParser, boolean paramBoolean, String paramString2)
  {
    this(paramString1);
    setStringParser(paramStringParser);
    setRequired(paramBoolean);
    setGreedy(false);
    setHelp(paramString2);
  }
  
  public UnflaggedOption setGreedy(boolean paramBoolean)
  {
    enforceParameterLock();
    greedy = paramBoolean;
    return this;
  }
  
  public UnflaggedOption setUsageName(String paramString)
  {
    _setUsageName(paramString);
    return this;
  }
  
  public boolean isGreedy()
  {
    return greedy;
  }
  
  public String getSyntax()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    if (!required()) {
      localStringBuffer.append("[");
    }
    String str = getUsageName();
    if (isGreedy()) {
      localStringBuffer.append(str + "1" + JSAP.SYNTAX_SPACECHAR + str + "2" + JSAP.SYNTAX_SPACECHAR + "..." + JSAP.SYNTAX_SPACECHAR + str + "N");
    } else {
      localStringBuffer.append("<" + str + ">");
    }
    if (!required()) {
      localStringBuffer.append("]");
    }
    return localStringBuffer.toString();
  }
  
  public UnflaggedOption setList(boolean paramBoolean)
  {
    super.internalSetList(paramBoolean);
    return this;
  }
  
  public UnflaggedOption setListSeparator(char paramChar)
  {
    super.internalSetListSeparator(paramChar);
    return this;
  }
  
  public UnflaggedOption setRequired(boolean paramBoolean)
  {
    super.internalSetRequired(paramBoolean);
    return this;
  }
  
  public UnflaggedOption setStringParser(StringParser paramStringParser)
  {
    super.internalSetStringParser(paramStringParser);
    return this;
  }
  
  public UnflaggedOption setDefault(String paramString)
  {
    _setDefault(paramString);
    return this;
  }
  
  public UnflaggedOption setDefault(String[] paramArrayOfString)
  {
    _setDefault(paramArrayOfString);
    return this;
  }
}
