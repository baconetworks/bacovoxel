package com.thevoxelbox.voxelsniper.jsap.jsap.ant;

public class DefaultValue
{
  private String value = null;
  
  public DefaultValue() {}
  
  public void addText(String paramString)
  {
    value = paramString;
  }
  
  public String getValue()
  {
    return value;
  }
}
