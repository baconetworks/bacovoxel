package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.net.MalformedURLException;
import java.net.URL;

public class URLStringParser
  extends StringParser
{
  private static final URLStringParser INSTANCE = new URLStringParser();
  
  public static URLStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public URLStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    URL localURL = null;
    try
    {
      localURL = new URL(paramString);
    }
    catch (MalformedURLException localMalformedURLException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to a URL.", localMalformedURLException);
    }
    return localURL;
  }
}
