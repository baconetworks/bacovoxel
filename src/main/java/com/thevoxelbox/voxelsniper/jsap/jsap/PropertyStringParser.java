package com.thevoxelbox.voxelsniper.jsap.jsap;

import java.util.Properties;

public abstract class PropertyStringParser
  extends StringParser
{
  private Properties properties = null;
  
  public PropertyStringParser() {}
  
  private void setProperties(Properties paramProperties)
  {
    properties = paramProperties;
  }
  
  private Properties getProperties()
  {
    if (properties == null) {
      setProperties(new Properties());
    }
    return properties;
  }
  
  public void setProperty(String paramString1, String paramString2)
  {
    Properties localProperties = getProperties();
    localProperties.setProperty(paramString1, paramString2);
  }
  
  public String getProperty(String paramString)
  {
    return getProperties().getProperty(paramString);
  }
  
  public String getProperty(String paramString1, String paramString2)
  {
    String str = getProperty(paramString1);
    return str == null ? paramString2 : str;
  }
}
