package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;

public class ByteStringParser
  extends StringParser
{
  private static final ByteStringParser INSTANCE = new ByteStringParser();
  
  public static ByteStringParser getParser()
  {
    return INSTANCE;
  }
  
  /**
   * @deprecated
   */
  public ByteStringParser() {}
  
  public Object parse(String paramString)
    throws ParseException
  {
    Byte localByte = null;
    try
    {
      localByte = Byte.decode(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new ParseException("Unable to convert '" + paramString + "' to a Byte.", localNumberFormatException);
    }
    return localByte;
  }
}
