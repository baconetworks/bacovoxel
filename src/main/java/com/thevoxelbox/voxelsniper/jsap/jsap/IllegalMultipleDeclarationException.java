package com.thevoxelbox.voxelsniper.jsap.jsap;

public class IllegalMultipleDeclarationException
  extends JSAPException
{
  private String id = null;
  
  public IllegalMultipleDeclarationException(String paramString)
  {
    super("Parameter '" + paramString + "' cannot be declared more than once.");
    id = paramString;
  }
  
  public String getID()
  {
    return id;
  }
}
