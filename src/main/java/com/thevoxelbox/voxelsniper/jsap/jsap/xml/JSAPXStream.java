package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

class JSAPXStream
  extends XStream
{
  public JSAPXStream()
  {
    super(new DomDriver());
    alias("jsap", JSAPConfig.class);
    alias("flaggedOption", FlaggedOptionConfig.class);
    alias("unflaggedOption", UnflaggedOptionConfig.class);
    alias("property", Property.class);
    alias("qualifiedSwitch", QualifiedSwitchConfig.class);
    alias("switch", SwitchConfig.class);
  }
}
