package com.thevoxelbox.voxelsniper.jsap.jsap.stringparsers;

import com.thevoxelbox.voxelsniper.jsap.jsap.ParseException;
import com.thevoxelbox.voxelsniper.jsap.jsap.StringParser;
import java.lang.reflect.Method;

public class ForNameStringParser
  extends StringParser
{
  private static final Class[] PARAMETERS = { String.class };
  private final Class klass;
  private final Method forName;
  
  private ForNameStringParser(Class paramClass)
    throws SecurityException, NoSuchMethodException
  {
    klass = paramClass;
    forName = paramClass.getMethod("forName", PARAMETERS);
  }
  
  public static ForNameStringParser getParser(Class paramClass)
    throws SecurityException, NoSuchMethodException
  {
    return new ForNameStringParser(paramClass);
  }
  
  public Object parse(String paramString)
    throws ParseException
  {
    try
    {
      return forName.invoke(klass, new Object[] { paramString });
    }
    catch (Exception localException)
    {
      throw new ParseException(localException);
    }
  }
}
