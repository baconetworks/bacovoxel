package com.thevoxelbox.voxelsniper.jsap.jsap.xml;

import com.thevoxelbox.voxelsniper.jsap.jsap.FlaggedOption;
import com.thevoxelbox.voxelsniper.jsap.jsap.JSAP;
import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import com.thevoxelbox.voxelsniper.jsap.jsap.Parameter;
import com.thevoxelbox.voxelsniper.jsap.jsap.QualifiedSwitch;

class FlaggedOptionConfig
  extends FlaggedConfig
{
  private boolean allowMultipleDeclarations = false;
  private boolean list = false;
  private char listSeparator = JSAP.DEFAULT_LISTSEPARATOR;
  private StringParserConfig stringParser = null;
  private boolean required = false;
  
  public FlaggedOptionConfig() {}
  
  public boolean allowMultipleDeclarations()
  {
    return allowMultipleDeclarations;
  }
  
  public void setAllowMultipleDeclarations(boolean paramBoolean)
  {
    allowMultipleDeclarations = paramBoolean;
  }
  
  public boolean isList()
  {
    return list;
  }
  
  public void setList(boolean paramBoolean)
  {
    list = paramBoolean;
  }
  
  public char getListSeparator()
  {
    return listSeparator;
  }
  
  public void setListSeparator(char paramChar)
  {
    listSeparator = paramChar;
  }
  
  public boolean isRequired()
  {
    return required;
  }
  
  public void setRequired(boolean paramBoolean)
  {
    required = paramBoolean;
  }
  
  public StringParserConfig getStringParser()
  {
    return stringParser;
  }
  
  public void setStringParser(StringParserConfig paramStringParserConfig)
  {
    stringParser = paramStringParserConfig;
  }
  
  protected void configure(QualifiedSwitch paramFlaggedOption)
  {
    super.configure(paramFlaggedOption);
    paramFlaggedOption.setUsageName(getUsageName());
    paramFlaggedOption.setShortFlag(getShortFlag());
    paramFlaggedOption.setLongFlag(getLongFlag());
    paramFlaggedOption.setAllowMultipleDeclarations(allowMultipleDeclarations());
    paramFlaggedOption.setListSeparator(getListSeparator());
    paramFlaggedOption.setList(isList());
    paramFlaggedOption.setRequired(isRequired());
    if (stringParser != null) {
      paramFlaggedOption.setStringParser(stringParser.getConfiguredStringParser());
    }
  }
  
  public Parameter getConfiguredParameter()
  {
    FlaggedOption localFlaggedOption = new FlaggedOption(getId());
    configure(localFlaggedOption);
    return localFlaggedOption;
  }
}
